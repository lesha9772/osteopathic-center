var map;
var center = {lat: 49.255919, lng: 28.473357};

var marker;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 49.255919, lng: 28.473357},
        zoom: 16
    });

    marker = new google.maps.Marker({
        position: center,
        title: 'Остеопатичний центр',
        map: map
    });
}



