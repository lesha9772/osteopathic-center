<?php

namespace app\components\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class IpBehavior extends Behavior {

  public $attributes = [
      ActiveRecord::EVENT_BEFORE_INSERT => 'ip',
      ActiveRecord::EVENT_BEFORE_UPDATE => 'ip',
  ];

  /**
   * Назначаем обработчик для [[owner]] событий
   * @return array события (array keys) с назначеными им обработчиками (array values)
   */
  public function events() {
    $events = $this->attributes;
    foreach ($events as $i => $event) {
      $events[$i] = 'getCurrentIp';
    }
    return $events;
  }

  /**
   * Добавляем IP адрес
   * @param Event $event Текущее событие
   */
  public function getCurrentIp($event) {
    $attributes = isset($this->attributes[$event->name]) ? (array) $this->attributes[$event->name] : [];

    if (!empty($attributes)) {
      foreach ($attributes as $source => $attribute) {
        $this->owner->$attribute = Yii::$app->request->userIP;
      }
    }
  }

}
