<?php

namespace app\components\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\Controller;
use app\components\widgets\tabgeowidget\TabGeoWidget;

class GeoBehavior extends Behavior {

  private $ip;
  private $country;
  public $default_country;
  public $attributes = [
      ActiveRecord::EVENT_BEFORE_INSERT => 'geo',
  ];

  public function __construct($config = []) {
    $this->ip = Yii::$app->request->userIP;
    parent::__construct($config);
  }

  /**
   * Назначаем обработчик для [[owner]] событий
   * @return array события (array keys) с назначеными им обработчиками (array values)
   */
  public function events() {
    $events = $this->attributes;
    foreach ($events as $i => $event) {
      $events[$i] = 'get_' . $event;
    }
    return $events;
  }

  /**
   * Get country by IP
   * @param Event $event 
   */
  public function get_geo($event) {
    $attributes = isset($this->attributes[$event->name]) ? (array) $this->attributes[$event->name] : [];
//
//    if (Yii::$app->request->userIP == '127.0.0.1') {
//      $this->ip = $this->test_ip;
    $this->ip = Yii::$app->request->userIP;
//    }
//    $this->country = 'ua';
    $this->country = TabGeoWidget::contry($this->ip);

    if (!empty($attributes)) {
      foreach ($attributes as $source => $attribute) {
        $this->owner->$attribute = $this->country;

        if (!$this->country)
          $this->owner->$attribute = $this->default_country;
        else
          $this->owner->$attribute = $this->country;
      }
    }
  }

}
