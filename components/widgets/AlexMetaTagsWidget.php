<?php

namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class AlexMetaTagsWidget extends Widget
{

  // for vk, 

  public $site_name;
  public $title; // описание, по рекомендациям 150-200 символов
  public $description; // описание, по рекомендациям 150-200 символов
  public $image; // картинка
  public $keywords; // ключевые слова
  public $url;
  public $domain;
  
//Мета-теги разметки Open graph
// site_name  public $og_site_name; //отвечает за название сайта:
  public $og_title; //
  public $og_description; //
  public $og_image; //content="http://euro-travel-example.com/thumbnail.jpg">Картинка должна иметь минимум 280 пикселей в ширину и 150 – в высоту. Вес картинки не должен превышать 1 Мб
  // $url public $og_url; //content="http://euro-travel-example.com/index.htm">Канонический URL страницы. Ссылка должна быть прямой, без дополнительных параметров, переменных, данных идентификации пользователей и т. д.
//    <!-- Мета-тег, указывающий, на используемый язык -->
//    <meta http-equiv="content-Language" content="Код языка">
  public $contentLanguage;
//    <!-- Meтa-тег для указания страны -->
//    <meta property = "business:contact_data:country_name" content = "Название страны">
  public $contact_data_country_name;
//    <!-- Мета-тег авторства -->
//    <meta name="author" content="Имя автора" />
  public $author;
//    <!-- Мета-тег указания авторских прав -->
//    <meta name="copyright" lang="ru" content="Имя автора" />
  public $copyright;
//    <!-- Мета-теги Google для указания контактной информации -->
//<meta property = "business:contact_data:postal_code" content = "Почтовый индекс">
//<meta property = "business:contact_data:email" content = "Емэйл">
//<meta property = "business:contact_data:phone_number" content = "телефон">
//<meta property = "business:contact_data:website" content = "URL веб сайта">
////<!-- Twitter -->
//<meta name="twitter:card" content="Тип карточки описания например - summary"/>
//<meta name="twitter:site" content="Название сайта"/>
//<meta name="twitter:title" content="Название страницы">
//<meta name="twitter:description" content="Описание"/>
//<meta name="twitter:creator" content="Автор"/>
//<meta name="twitter:image:src" content="Url изображения"/>
//<meta name="twitter:domain" content="Url адрес сайта"/>    

  public $twitter_site;
  public $twitter_title;
  public $twitter_description;
  public $twitter_creator;
  public $twitter_image_src;
  public $twitter_domain = '';
//  <!-- Facebook --> из опен граф  
//<meta property="og:type" content="profile"/>
//<meta property="profile:first_name" content="Имя"/>
//<meta property="profile:last_name" content="Фамилия"/>
//<meta property="profile:username" content="Ник"/>
//<meta property="og:title" content="Название страницы"/>
//<meta property="og:description" content="Описание"/>
//<meta property="og:image" content="Url изображения"/>
//<meta property="og:url" content="Url сайта"/>
//<meta property="og:site_name" content="Название сайта"/>
//<meta property="og:see_also" content="Url зеркала сайта"/>
//<meta property="fb:admins" content="Facebook_ID"/>
//  public $fb_
//  <!-- Мета-тег Google для запрета перевода на какой-либо из языков -->
//<meta name="google" content="notranslate" />
  public $notranslate;

  public function init()
  {
    parent::init();
//        if ($this->title === null) {
//            $this->message = 'Hello World';
//        }
  }

  public function run()
  {

    $this->withoutOptionsMetaTag();


    return true;
  }

  private function withoutOptionsMetaTag()
  {

    if ($this->title !== null) {
      $this->registrationMetaTag('title', $this->title);
    }
    if ($this->description !== null) {
      $this->registrationMetaTag('description', $this->description);
    }

    if ($this->keywords !== null) {
      $this->registrationMetaTag('keywords', $this->keywords);
    }


    //Мета-теги разметки Open graph
    if ($this->site_name !== null) {
      $this->registrationMetaTag('og:site_name', $this->site_name);
    }
    if ($this->title !== null) {
      $this->registrationMetaTag('og:title', $this->title);
    }
    if ($this->description !== null) {
      $this->registrationMetaTag('og:description', $this->description);
    }
    if ($this->image !== null) {
      $this->registrationMetaTag('og:image', $this->image);
    }
    if ($this->url !== null) {
      $this->registrationMetaTag('og:url', $this->url);
    }


//    Мета-теги Twitter 
    if ($this->site_name !== null) {
      $this->registrationMetaTag('twitter:site', $this->site_name);
    }
    if ($this->title !== null) {
      $this->registrationMetaTag('twitter:title', $this->title);
    }
    if ($this->description !== null) {
      $this->registrationMetaTag('twitter:description', $this->description);
    }
    if ($this->image !== null) {
      $this->registrationMetaTag('twitter:image:src', $this->image);
    }
    if ($this->domain !== null) {
      $this->registrationMetaTag('twitter:domain', $this->domain);
    }
  }

  /**
   * 
   * Function for registration meta-tags
   * 
   * @param string $name
   * @param string $content
   */
  private function registrationMetaTag($name, $content)
  {

    \Yii::$app->view->registerMetaTag([
        'name' => $name,
        'content' => $content,
    ]);
  }

}
