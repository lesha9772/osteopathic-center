<?php

namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class ShareLinksWidget extends Widget {

  /**
   * Constants representing each socil net
   */
  const SOCIAL_TWITTER = 'twitter';
  const SOCIAL_FACEBOOK = 'facebook';
  const SOCIAL_VKONTAKTE = 'vkontakte';
  const SOCIAL_PINTEREST = 'pinterest';
  const SOCIAL_GPLUS = 'gplus';
  const SOCIAL_LINKEDIN = 'linkedin';
  const SOCIAL_KINDLE = 'kindle';
  const SOCIAL_XING = 'xing';
  const SOCIAL_ODNOKLASSNIKI = 'odnoklassniki';

  /**
   * Url to share
   *
   * @var string
   */
  public $url = 'https://github.com/dirkgroenen/Pinterest-API-PHP';
  public $network;

  /**
   * Sharing url template for each network
   *
   * @var array
   */
  public $shareUrlMap = [
      self::SOCIAL_TWITTER => 'https://twitter.com/intent/tweet?url={url}',
      self::SOCIAL_FACEBOOK => 'https://www.facebook.com/sharer/sharer.php?u={url}',
      self::SOCIAL_VKONTAKTE => 'http://vk.com/share.php?url={url}',
      self::SOCIAL_GPLUS => 'https://plus.google.com/share?url={url}',
      self::SOCIAL_LINKEDIN => 'http://www.linkedin.com/shareArticle?url={url}',
      self::SOCIAL_KINDLE => 'http://fivefilters.org/kindle-it/send.php?url={url}',
      self::SOCIAL_XING => 'https://www.xing.com/spi/shares/new?url={url}',
      self::SOCIAL_ODNOKLASSNIKI => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1?st._surl={url}'
  ];

  public function init() {
    parent::init();
  }

  public function run() {
    $url = $this->shareUrl();
    return $url;
  }

  private function shareUrl() {
    $url = $this->url;
    return str_replace('{url}', urlencode($url), ArrayHelper::getValue($this->shareUrlMap, $this->network));
  }

}
