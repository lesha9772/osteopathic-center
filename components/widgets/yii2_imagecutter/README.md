Yii2-ImageCutter
================
Yii2-ImageCutter

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist lesha9772/yii2-imagecutter "*"
```

or add

```
"lesha9772/yii2-imagecutter": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \lesha9772\imagecutter\AutoloadExample::widget(); ?>```