<?php

namespace app\components\widgets\yii2_imagecutter\behaviors;

use Yii;
use yii\base\Behavior;
use yii\helpers\Json;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Imagine\Image\Palette\Color;
use yii\web\NotFoundHttpException;
//use Imagine\Image\Color;
/**
 * Class CutterBehavior
 * @package sadovojav\cutter\behavior
 */
class CutterBehavior extends \yii\behaviors\AttributeBehavior {

  /**
   * Attributes
   * @var
   */
  public $attributes;

  /**
   * Base directory
   * @var
   */
  public $baseDir;

  /**
   * Base directory of small images
   * @var
   */
  public $baseSmallDir = 'small/';

  /**
   * Base path
   * @var
   */
  public $basePath;
  public $quality = 92;

  public function events() {
    return [
        ActiveRecord::EVENT_BEFORE_INSERT => 'beforeUpload',
        ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
        ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
    ];
  }

  public function beforeUpload() {
    if (is_array($this->attributes) && count($this->attributes)) {
      foreach ($this->attributes as $attribute) {
        $this->upload($attribute);
      }
    } else {
      $this->upload($this->attributes);
    }
  }

  public function beforeUpdate() {
    if (is_array($this->attributes) && count($this->attributes)) {
      foreach ($this->attributes as $attribute) {

        if ($uploadImage = UploadedFile::getInstance($this->owner, $attribute)) {
          $this->delete($attribute);
        }
        $this->upload($attribute);
      }
    } else {
      $this->upload($this->attributes);
    }
  }

  public function upload($attribute) {


    if ($uploadImage = UploadedFile::getInstance($this->owner, $attribute)) {

      $cropping = $_POST[$attribute . '-cropping'];

      $croppingFileName = Yii::$app->security->generateRandomString(10) . '_' . time();
      $croppingFileExt = strrchr($uploadImage->name, '.');
      $croppingFileDir = substr($croppingFileName, 0, 2);

      $croppingFileBasePath = Yii::getAlias($this->basePath) . $this->baseDir;

//      var_dump($croppingFileBasePath);
//      die;

      if (!is_dir($croppingFileBasePath)) {
        mkdir($croppingFileBasePath, 0755, true);
      }

      $croppingFilePath = Yii::getAlias($this->basePath) . $this->baseDir;

      if (!is_dir($croppingFilePath)) {
        mkdir($croppingFilePath, 0755, true);
      }
      $smallDirPath = Yii::getAlias($this->basePath) . $this->baseDir . DIRECTORY_SEPARATOR . $this->baseSmallDir;
      if (!is_dir($smallDirPath)) {
        mkdir($smallDirPath, 0755, true);
      }

      $croppingFile = $croppingFilePath . DIRECTORY_SEPARATOR . $croppingFileName . $croppingFileExt;

      $imageTmp = Image::getImagine()->open($uploadImage->tempName);
      $imageTmp->rotate($cropping['dataRotate']);

//      $background = new Color('#F2F19E');
//      $background = '#F2F19E';
      $image = Image::getImagine()->create($imageTmp->getSize());
      $image->paste($imageTmp, new Point(0, 0));

      $point = new Point($cropping['dataX'], $cropping['dataY']);
      $box = new Box($cropping['dataWidth'], $cropping['dataHeight']);

      $image->crop($point, $box);
      $image->save($croppingFile, ['quality' => $this->quality]);

      $this->owner->{$attribute} = $croppingFileName . $croppingFileExt;
    } elseif (isset($_POST[$attribute . '-remove']) && $_POST[$attribute . '-remove']) {
      $this->delete($attribute);
    } elseif (isset($this->owner->oldAttributes[$attribute])) {
      $this->owner->{$attribute} = $this->owner->oldAttributes[$attribute];
    }else{
       
          throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function beforeDelete() {
    if (is_array($this->attributes) && count($this->attributes)) {
      foreach ($this->attributes as $attribute) {
        $this->delete($attribute);
      }
    } else {
      $this->delete($this->attributes);
    }
  }

  public function delete($attribute) {
    $file = Yii::getAlias($this->basePath) . $this->baseDir . DIRECTORY_SEPARATOR . $this->owner->oldAttributes[$attribute];

//    var_dump($file);
//    die();

    if (is_file($file) && file_exists($file)) {
      unlink($file);
    } else {
      Yii::$app->session->setFlash('error', 'Error deleting original image');
    }

// get name image
    $ext_arr = explode(".", $this->owner->oldAttributes[$attribute]);
    $imageNameText = $ext_arr[0];


    if (is_dir(Yii::getAlias($this->basePath) . $this->baseDir . DIRECTORY_SEPARATOR . $this->baseSmallDir)) {


      $allSmallImages = scandir(Yii::getAlias($this->basePath) . $this->baseDir . DIRECTORY_SEPARATOR . $this->baseSmallDir);

      $dirSmallImages = Yii::getAlias($this->basePath) . $this->baseDir . DIRECTORY_SEPARATOR . $this->baseSmallDir;
      foreach ($allSmallImages as $singleSmImage) {
        $pos = stripos($singleSmImage, $imageNameText);
        if ($pos !== false) {
          if (file_exists($dirSmallImages . '/' . $singleSmImage)) {
            if (!unlink($dirSmallImages . '/' . $singleSmImage)) {
              Yii::$app->session->setFlash('error', 'Error deleting small images');
            }
          } else {
            Yii::$app->session->setFlash('error', 'Error deleting small images');
          }
        }
      }
    }

    $this->owner->{$attribute} = '';
  }

}
