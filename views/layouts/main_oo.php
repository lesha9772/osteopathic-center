<!DOCTYPE html>
<!-- saved from url=(0030)# -->
<html lang="en-gb" dir="ltr" slick-uniqueid="3" class="g-offcanvas-css3">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<base href="">-->
        <base href=".">
        <meta name="generator" content="Joomla! - Open Source Content Management">
        <title>Головна</title>
        <link href="index.php" rel="canonical">
        <link rel="stylesheet" href="/css2/rokbox.css" type="text/css">
        <link rel="stylesheet" href="/css2/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/css2/nucleus.css" type="text/css">
        <link rel="stylesheet" href="/css2/photon_73.css" type="text/css">
        <link rel="stylesheet" href="/css2/photon-joomla_73.css" type="text/css">
        <link rel="stylesheet" href="/css2/custom_73.css" type="text/css">
        <link rel="stylesheet" href="/css2/demo.css" type="text/css">
        <link rel="stylesheet" href="/css2/animate.css" type="text/css">
        <style type="text/css">
            #g-owlcarousel-module-owlcarousel-507 .g-owlcarousel-item-img:after {
                background: rgba(0, 0, 0, 0);
                background: -webkit-linear-gradient(rgba(0, 0, 0, 0), #20232a);
                background: -o-linear-gradient(rgba(0, 0, 0, 0), #20232a);
                background: -moz-linear-gradient(rgba(0, 0, 0, 0), #20232a);
                background: linear-gradient(rgba(0, 0, 0, 0), #20232a);
            }
            #g-owlcarousel-module-owlcarousel-504 .owl-dots .owl-dot:nth-child(1) {
                background: url(img2/avatar-01.jpg);
                background-size: cover;
            }
            #g-owlcarousel-module-owlcarousel-504 .owl-dots .owl-dot:nth-child(2) {
                background: url(img2/avatar-02.jpg);
                background-size: cover;
            }
            #g-owlcarousel-module-owlcarousel-504 .owl-dots .owl-dot:nth-child(3) {
                background: url(/img2/avatar-03.jpg);
                background-size: cover;
            }
        </style>
        <script src="/js2/mootools-core.js" type="text/javascript"></script>
        <script src="/js2/core.js" type="text/javascript"></script>
        <script src="/js2/mootools-more.js" type="text/javascript"></script>
        <script src="/js2/rokbox.js" type="text/javascript"></script>
        <script src="/js2/jquery.min.js" type="text/javascript"></script>
        <script src="/js2/jquery-noconflict.js" type="text/javascript"></script>
        <script src="/js2/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="/js2/juitabs.js" type="text/javascript"></script>
        <script type="text/javascript">
          if (typeof RokBoxSettings == 'undefined') RokBoxSettings = {pc: '100'};
        </script>
    </head>
    <body class="gantry site com_gantry5 view-custom no-layout no-task dir-ltr itemid-101 outline-73 g-offcanvas-left g-home-positions g-style-preset2">
        <div id="g-offsidebar-overlay"></div>
        <div id="g-offcanvas" data-g-offcanvas-swipe="1" data-g-offcanvas-css3="1">
            <div class="g-grid">
                <div class="g-block hidden size-100">
                    <div class="g-content">
                        <div id="g-mobilemenu-container" data-g-menu-breakpoint="48rem"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="g-page-surround">
            <div class="g-menu-overlay"></div>
            <div class="g-offcanvas-hide g-offcanvas-toggle" data-offcanvas-toggle=""><i class="fa fa-fw fa-bars"></i></div>
            <section id="g-top">
                <div class="g-container">
                    <div class="g-grid">
                        <div class="g-block size-100">
                            <div class="g-system-messages">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="g-navigation">
                <div class="g-container">
                    <div class="g-grid">
                        <div class="g-block size-20">
                            <div class="g-content">
                                <a href="" title="VinOst" rel="home" class="g-logo">
                                    VinOst
                                </a>
                            </div>
                        </div>
                        <div class="g-block size-60">
                            <div class="g-content">
                                <nav class="g-main-nav" role="navigation">
                                    <ul class="g-toplevel">
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-101 active g-standard  ">
                                            <a class="g-menu-item-container" href="index.php">
                                                <span class="g-menu-item-content">
                                                    <span class="g-menu-item-title">Головна</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-194 g-menu-item-type-url g-standard">
                                            <div class="g-menu-item-container" data-g-menuparent="">                                                                            <span class="g-separator g-menu-item-content">            <span class="g-menu-item-title">Що таке остеопатія?</span>
                                                </span>
                                            </div>
                                        </li>
                                        <li class="g-menu-item g-menu-item-193 g-menu-item-type-url g-standard">
                                            <div class="g-menu-item-container" data-g-menuparent="">                                                                            <span class="g-separator g-menu-item-content">            <span class="g-menu-item-title">Методи Лікування</span>
                                                </span>
                                            </div>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-115 g-standard  ">
                                            <a class="g-menu-item-container" href="index.php/portfolio">
                                                <span class="g-menu-item-content">
                                                    <span class="g-menu-item-title">Галерея</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-114 g-standard  ">
                                            <a class="g-menu-item-container" href="index.php/pricing">
                                                <span class="g-menu-item-content">
                                                    <span class="g-menu-item-title">Прайс</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-192 g-standard  ">
                                            <a class="g-menu-item-container" href="#">
                                                <span class="g-menu-item-content">
                                                    <span class="g-menu-item-title">Контакти</span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="g-block hidden-phone size-20">
                            <div class="g-content">
                                <div class="g-social align-right">
                                    <a target="_blank" href="http://www.facebook.com/" title="">
                                        <span class="fa fa-facebook fa-fw"></span>
                                        <span class="g-social-text"></span>
                                    </a>
                                    <a target="_blank" href="http://www.twitter.com/" title="">
                                        <span class="fa fa-twitter fa-fw"></span>
                                        <span class="g-social-text"></span>
                                    </a>
                                    <a target="_blank" href="https://plus.google.com/" title="">
                                        <span class="fa fa-google-plus fa-fw"></span>
                                        <span class="g-social-text"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="g-slideshow">
                <div class="g-container">
                    <div class="g-grid">
                        <div class="g-block nomarginbottom nopaddingbottom size-100">
                            <div class="g-content">
                                <div class="moduletable g-bottom-offset">
                                    <div class="">
                                        <div class="g-newsslider" id="module-newsslider-498">
                                            <div class="g-newsslider-preview news-preview">
                                                <div class="g-newsslider-content news-content" style="background-image: url('img2/img-01.jpeg');">
                                                    <div class="g-newsslider-overlay">
                                                        <div class="g-newsslider-preview-title">Наша Методика</div>
                                                        <span class="g-newsslider-button"> <a target="_self" href="#" title="Детальніше" class="button ">Детальніше</a> </span>
                                                    </div>
                                                </div>
                                                <div class="g-newsslider-content news-content" style="background-image: url('img2/img-02.jpg');">
                                                    <div class="g-newsslider-overlay">
                                                        <div class="g-newsslider-preview-title">Остеопатія</div>
                                                        <div class="g-newsslider-preview-desc">апывапыпвапывапывапвапы апва п ва вапапы ап ывап вап ывап ывап ывап ываппывапы</div>
                                                        <span class="g-newsslider-button"> <a target="_self" href="" title="Детальніше" class="button ">Детальніше</a> </span>
                                                    </div>
                                                </div>
                                                <div class="g-newsslider-content news-content" style="background-image: url('img2/img-03.jpg');">
                                                    <div class="g-newsslider-overlay">
                                                        <div class="g-newsslider-preview-title">Кінезітерапія</div>
                                                        <span class="g-newsslider-button"> <a target="_self" href="" title="Детальніше" class="button ">Детальніше</a> </span>
                                                    </div>
                                                </div>
                                                <div class="g-newsslider-content news-content" style="background-image: url('img2/img-04.jpg');">
                                                    <div class="g-newsslider-overlay">
                                                        <div class="g-newsslider-preview-title">Масажні Техніки</div>
                                                        <span class="g-newsslider-button"> <a target="_self" href="" title="Детальніше" class="button ">Детальніше</a> </span>
                                                    </div>
                                                </div>
                                                <div class="g-newsslider-content news-content" style="background-image: url('img2/img-05.jpg');">
                                                    <div class="g-newsslider-overlay">
                                                        <div class="g-newsslider-preview-title">ФІзіотерапія</div>
                                                        <span class="g-newsslider-button"> <a target="_self" href="" title="Детальніше" class="button ">Детальніше</a> </span>
                                                    </div>
                                                </div>
                                                <div class="g-newsslider-content news-content top-content" style="">
                                                    <div class="g-newsslider-overlay">
                                                        <div class="g-newsslider-preview-title">Йога-терапія</div>
                                                        <span class="g-newsslider-button"> <a target="_self" href="" title="Детальніше" class="button ">Детальніше</a> </span>
                                                    </div>
                                                </div>
                                                <ul class="g-newsslider-pagination">
                                                    <li class="">1</li>
                                                    <li class="">2</li>
                                                    <li class="">3</li>
                                                    <li class="">4</li>
                                                    <li class="">5</li>
                                                    <li class="selected">6</li>
                                                </ul>
                                            </div>
                                            <div id="g-newsslider-scrollbar-module-newsslider-498" class="g-newsslider-scrollbar">
                                                <ul class="g-newsslider-headlines news-headlines">
                                                    <li class="nh-anim first">
                                                        <div class="arrow-container">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </div>
                                                        <span class="g-newsslider-headlines-title">Наша Методика</span>
                                                    </li>
                                                    <li class="nh-anim">
                                                        <div class="arrow-container">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </div>
                                                        <span class="g-newsslider-headlines-title">Остеопатія</span>
                                                    </li>
                                                    <li class="nh-anim">
                                                        <div class="arrow-container">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </div>
                                                        <span class="g-newsslider-headlines-title">Кінезітерапія</span>
                                                    </li>
                                                    <li class="nh-anim">
                                                        <div class="arrow-container">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </div>
                                                        <span class="g-newsslider-headlines-title">Масажні Техніки</span>
                                                    </li>
                                                    <li class="nh-anim">
                                                        <div class="arrow-container">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </div>
                                                        <span class="g-newsslider-headlines-title">ФІзіотерапія</span>
                                                    </li>
                                                    <li class="nh-anim selected">
                                                        <div class="arrow-container">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </div>
                                                        <span class="g-newsslider-headlines-title">Йога-терапія</span>
                                                    </li>
                                                </ul>
                                                <div class="g-newsslider-navigation clearfix">
                                                    <div class="prev"><i class="fa fa-arrow-up"></i></div>
                                                    <div class="next"><i class="fa fa-arrow-down"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="g-container">
                <header id="g-header" class="g-offset-padding">
                    <div class="g-grid">
                        <div class="g-block size-68">
                            <div class="g-content">
                                <div class="moduletable ">
                                    <h2 class="g-title">Міні Галерея</h2>
                                    <div class="g-imagegrid ">
                                        <div class="g-imagegrid-desc">Ласкаво-просимо до нашого остеопатичного центру! Пропонуємо Вам ознайомитися з останніми світлинами з нашої фото-галереї.</div>
                                        <div class="g-imagegrid-wrapper g-imagegrid-5cols">
                                            <div class="g-imagegrid-item">
                                                <a class="g-imagegrid-link" href="img2/img-01.jpg" data-rokbox="" data-rokbox-album="" data-rokbox-caption="">
                                                    <img src="img2/img-01.jpg" alt="">
                                                    <div class="indicator">
                                                        <div>+</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="g-imagegrid-item">
                                                <a class="g-imagegrid-link" href="img2/img-02.jpg" data-rokbox="" data-rokbox-album="" data-rokbox-caption="">
                                                    <img src="img2/img-02.jpg" alt="">
                                                    <div class="indicator">
                                                        <div>+</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="g-imagegrid-item">
                                                <a class="g-imagegrid-link" href="img2/img-03.jpg" data-rokbox="" data-rokbox-album="" data-rokbox-caption="">
                                                    <img src="img2/img-03.jpg" alt="">
                                                    <div class="indicator">
                                                        <div>+</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="g-imagegrid-item">
                                                <a class="g-imagegrid-link" href="img2/img-04.jpg" data-rokbox="" data-rokbox-album="" data-rokbox-caption="">
                                                    <img src="img2/img-04.jpg" alt="">
                                                    <div class="indicator">
                                                        <div>+</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="g-imagegrid-item">
                                                <a class="g-imagegrid-link" href="img2/img-05.jpg" data-rokbox="" data-rokbox-album="" data-rokbox-caption="">
                                                    <img src="img2/img-05.jpg" alt="">
                                                    <div class="indicator">
                                                        <div>+</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="g-block nomarginall nopaddingall size-32">
                            <div class="g-content">
                                <div class="moduletable ">
                                    <div class="">
                                        <div id="g-pricingtable-module-pricingtable-502" class="g-pricingtable-container g-pricingtable-1-col">
                                            <div class="g-pricingtable-col-item g-pricingtable-accent1" id="g-pricingtable-col-item-1">
                                                <ul class="g-pricingtable ">
                                                    <li class="g-pricingtable-plan">
                                                        <a target="_self" href="#" title="Записатися на Прийом">                Консультація Лікаря
                                                        </a>            
                                                    </li>
                                                    <li class="g-pricingtable-price">100.00 грн. <span class="g-pricingtable-period"></span></li>
                                                    <li class="g-pricingtable-item g-pricingtable-item-0 "><i class="fa fa-check"></i> Досвідчені Лікарі</li>
                                                    <li class="g-pricingtable-item g-pricingtable-item-1 "><i class="fa fa-check"></i> Сучасні Методики</li>
                                                    <li class="g-pricingtable-cta"><a target="_self" href="#" title="Записатися на Прийом" class="button "><i class="fa fa-arrow-right"></i> Записатися на Прийом</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <div class="g-container">
                <section id="g-showcase" class="g-compact">
                    <div class="g-grid">
                        <div class="g-block size-68">
                            <div class="g-content">
                                <div class="moduletable ">
                                    <div class="">
                                        <ul class="accordion" id="module-accordion-467">
                                            <li id="accordion-id1" class="active" style="opacity: 1;">
                                                <div class="accordion-item-title active" style="opacity: 1;">
                                                    Які, є протипоказання? &nbsp;Ірина 33 роки 
                                                    <div class="toggle active" style="opacity: 1;"><i class="fa fa-minus"></i></div>
                                                </div>
                                                <div class="accordion-item-content active" style="display: block; opacity: 1;">
                                                    З радістю відповімо на Ваші запитання.
                                                </div>
                                            </li>
                                            <li id="accordion-id2">
                                                <div class="accordion-item-title">
                                                    Ви працюєте в Суботу? &nbsp;Ольга 24 роки 
                                                    <div class="toggle"><i class="fa fa-plus"></i></div>
                                                </div>
                                                <div class="accordion-item-content" style="display: none;">
                                                    З радістю відповімо на Ваші запитання.
                                                </div>
                                            </li>
                                            <li id="accordion-id3">
                                                <div class="accordion-item-title">
                                                    Які, є протипоказання? &nbsp;Ірина 49 років 
                                                    <div class="toggle"><i class="fa fa-plus"></i></div>
                                                </div>
                                                <div class="accordion-item-content" style="display: none;">
                                                    З радістю відповімо на Ваші запитання.
                                                </div>
                                            </li>
                                            <li id="accordion-id4">
                                                <div class="accordion-item-title">
                                                    Які, є протипоказання? &nbsp;Ірина 49 років 
                                                    <div class="toggle"><i class="fa fa-plus"></i></div>
                                                </div>
                                                <div class="accordion-item-content" style="display: none;">
                                                    З радістю відповімо на Ваші запитання.
                                                </div>
                                            </li>
                                            <li id="accordion-id5">
                                                <div class="accordion-item-title">
                                                    Які, є протипоказання? &nbsp;Вадим 55 років 
                                                    <div class="toggle"><i class="fa fa-plus"></i></div>
                                                </div>
                                                <div class="accordion-item-content" style="display: none;">
                                                    З радістю відповімо на Ваші запитання.
                                                </div>
                                            </li>
                                            <li id="accordion-id6">
                                                <div class="accordion-item-title">
                                                    Які, є протипоказання? &nbsp;Ірина 52 роки? 
                                                    <div class="toggle"><i class="fa fa-plus"></i></div>
                                                </div>
                                                <div class="accordion-item-content" style="display: none;">
                                                    З радістю відповімо на Ваші запитання.
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="g-block nomarginall nopaddingall size-32">
                            <div class="g-content">
                                <div class="moduletable box3 nomarginall contactinfo">
                                    <h4>Вінниця, вул. Київська 104&nbsp;</h4>
                                    <p class="desc">0432 69-00-00 <br>093 000-00-00<br>093 000-00-00</p>
                                    <p
                                        <a href="mailto:osteovin@gmail.com" class="email">
                                            osteovin@gmail.com
                                            <p></p>
                                    </p
                                    <a>
                                </div>
                                <div class="moduletable nopaddingall nomarginall">
                                    <div class="g-gridcontent ">
                                        <div class="g-grid">
                                            <div class="g-block g-gridcontent-title-desc">
                                            </div>
                                            <div class="g-block g-gridcontent-readmore">
                                            </div>
                                        </div>
                                        <div class="g-gridcontent-wrapper g-gridcontent-2cols">
                                            <div class="g-gridcontent-item">
                                                <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                    <i class="fa fa-cog"></i>
                                                    <span class="g-gridcontent-item-title">Як добратися?</span>
                                                </div>
                                            </div>
                                            <div class="g-gridcontent-item">
                                                <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                    <i class="fa fa-cubes"></i>
                                                    <span class="g-gridcontent-item-title">Важливо знати!</span>
                                                </div>
                                            </div>
                                            <div class="g-gridcontent-item">
                                                <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                    <i class="fa fa-columns"></i>
                                                    <span class="g-gridcontent-item-title">Відгуки пацієнтів</span>
                                                </div>
                                            </div>
                                            <div class="g-gridcontent-item">
                                                <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                    <i class="fa fa-copy"></i>
                                                    <span class="g-gridcontent-item-title">Фото Галерея</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>		</div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="g-container">            <section id="g-expanded" class="nomarginbottom">
                    <div class="g-grid">
                        <div class="g-block size-68">
                            <div class="g-content">
                                <div class="moduletable ">
                                    <div class="">
                                        <div class="g-contenttabs">
                                            <div id="g-contenttabs-module-contenttabs-506" class="g-contenttabs-container ui-tabs ui-widget ui-widget-content ui-corner-all">
                                                <ul class="g-contenttabs-tab-wrapper-container ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                                    <li class="g-contenttabs-tab-wrapper ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="Particles" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
                                                        <span class="g-contenttabs-tab-wrapper-head">
                                                            <a class="g-contenttabs-tab ui-tabs-anchor" href="#Particles" role="presentation" tabindex="-1" id="ui-id-1">
                                                                <span class="g-contenttabs-tab-title">Particles</span>
                                                                <span class="g-contenttabs-tab-subtitle"></span>
                                                            </a>
                                                        </span>
                                                    </li>
                                                    <li class="g-contenttabs-tab-wrapper ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="Menus" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                                                        <span class="g-contenttabs-tab-wrapper-head">
                                                            <a class="g-contenttabs-tab ui-tabs-anchor" href="#Menus" role="presentation" tabindex="-1" id="ui-id-2">
                                                                <span class="g-contenttabs-tab-title">Menus</span>
                                                                <span class="g-contenttabs-tab-subtitle"></span>
                                                            </a>
                                                        </span>
                                                    </li>
                                                    <li class="g-contenttabs-tab-wrapper ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="Layout" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
                                                        <span class="g-contenttabs-tab-wrapper-head">
                                                            <a class="g-contenttabs-tab ui-tabs-anchor" href="#Layout" role="presentation" tabindex="-1" id="ui-id-3">
                                                                <span class="g-contenttabs-tab-title">Layout</span>
                                                                <span class="g-contenttabs-tab-subtitle"></span>
                                                            </a>
                                                        </span>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                                <ul class="g-contenttabs-content-wrapper-container">
                                                    <li class="g-contenttabs-tab-wrapper">
                                                        <div class="g-contenttabs-tab-wrapper-body">
                                                            <div id="Particles" class="g-contenttabs-content ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">News Slider</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">1</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Highlight top headlines without taking up a lot of vertical space. This particle is perfect for breaking news and important information.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Content Tabs</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">2</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Give your content a clean, organized look by splitting it into elegant, easy-to-navigate tabs.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Owl Carousel</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">3</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Showcase images and headline your biggest features with this gorgeous, simple particle.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Pricing Table</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">4</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Create clean, modern pricing tables that enable you to compare features and offerings from different pricing categories. Perfect for e-commerce.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Accordion</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">5</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Create lists with expanding and contracting line items with ease. The Accordion particle makes it easy to maximize your available space.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Image Grid</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">6</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Showcase your images in a modern, dynamic way with this particle.</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="g-contenttabs-tab-wrapper">
                                                        <div class="g-contenttabs-tab-wrapper-body">
                                                            <div id="Menus" class="g-contenttabs-content ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Multi Column</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">1</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Split your submenus into multiple columns for a clean, easy-to-navigate look.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Icon Picker</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">2</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Add personality to your menu with icons using Gantry 5’s Icon Picker.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Custom Width</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">3</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Want to set a custom width for your menu? No problem!</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Dropdown</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">4</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Control virtually every aspect of your menu, including the direction submenus drop to from the Menu Editor.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Module Picker</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">5</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Inject modules directly into your menu quickly and easily using the Module Picker.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Insert Particles</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">6</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Particles can be injected directly into your menu in seconds!</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="g-contenttabs-tab-wrapper">
                                                        <div class="g-contenttabs-tab-wrapper-body">
                                                            <div id="Layout" class="g-contenttabs-content ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" style="display: none;">
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Drag and Drop</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">1</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>The Layout Manager features an infinitely simple drag-and-drop interface, enabling you to edit your layout from your desktop, laptop, or tablet.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Particle Picker</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">2</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Add particles directly to your layout with drag-and-drop simplicity.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Section Classes</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent1"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">3</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Give each section of your page a unique look by applying classes directly using the Layout Manager. </span>
                                                                    </div>
                                                                </div>
                                                                <div class="g-contenttabs-tag">
                                                                    <div class="g-contenttabs-content-tag">
                                                                        <span class="g-contenttabs-content-tag-text">Positions</span>
                                                                        <span class="g-contenttabs-content-subtag-dot g-contenttabs-content-subtag-dot-accent2"></span>
                                                                        <span class="g-contenttabs-content-subtag-text">4</span>
                                                                    </div>
                                                                    <div class="g-contenttabs-content-text">
                                                                        <span>Give a section one, two, or twenty rows! Fill them with particles and module positions to your heart’s desire. There is really no limit to what you can do with Gantry 5’s Layout Manager!</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>		</div>
                            </div>
                        </div>
                        <div class="g-block nomarginall nopaddingall size-32">
                            <div class="g-content">
                                <div class="moduletable nomarginall nopaddingall owlcarousel-offset">
                                    <div class="">
                                        <div id="g-owlcarousel-module-owlcarousel-507" class="g-owlcarousel owl-carousel g-owlcarousel-fullwidth owl-theme owl-loaded">
                                            <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1836px, 0px, 0px); transition: 0.25s; width: 2754px;"><div class="owl-item cloned" style="width: 459px; margin-right: 0px;"><div>
                                                            <img src="img2/img-01(1).jpg" alt="">
                                                        </div></div><div class="owl-item cloned" style="width: 459px; margin-right: 0px;"><div>
                                                            <img src="img2/img-02(1).jpg" alt="">
                                                        </div></div><div class="owl-item" style="width: 459px; margin-right: 0px;"><div>
                                                            <img src="img2/img-01(1).jpg" alt="">
                                                        </div></div><div class="owl-item" style="width: 459px; margin-right: 0px;"><div>
                                                            <img src="img2/img-02(1).jpg" alt="">
                                                        </div></div><div class="owl-item cloned active" style="width: 459px; margin-right: 0px;"><div>
                                                            <img src="img2/img-01(1).jpg" alt="">
                                                        </div></div><div class="owl-item cloned" style="width: 459px; margin-right: 0px;"><div>
                                                            <img src="img2/img-02(1).jpg" alt="">
                                                        </div></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style=""><i class="fa fa-chevron-left"></i></div><div class="owl-next" style=""><i class="fa fa-chevron-right"></i></div></div><div class="owl-dots" style="display: none;"></div></div></div>
                                    </div>		</div><div class="moduletable box-white nomarginall g-offset-padding g-bottom-offset">
                                    <div class="">
                                        <div class="g-simplecontent">
                                            <div class="g-simplecontent-item g-simplecontent-layout-standard">
                                                <div class="g-simplecontent-item-created-date">15 05 2016</div>
                                                <div class="g-simplecontent-item-content-title">Про Центр</div>
                                                <div class="g-simplecontent-item-leading-content">Гарантуємо відповідність надання медичних послуг європейським стандартам, якість та відповідність медичного обладнання сучасним вимогам, відповідну кваліфікацію та сертифікацію персоналу. Жорсткий санітарний контроль; систему безпеки пацієнтів під час проведення маніпуляцій та процедур.</div>        
                                                <div class="g-simplecontent-item-readmore-container">
                                                    <a target="_self" href="" class="g-simplecontent-item-readmore button">Детальніше</a>
                                                </div>
                                            </div>
                                        </div>		</div>
                                </div>
                            </div>
                        </div>
                    </div></section>
            </div>
            <div class="g-container">
                <section id="g-extension" class="nomargintop">
                    <div class="g-grid">
                        <div class="g-block nomarginall nopaddingall size-100">
                            <div class="g-content">
                                <div class="moduletable ">
                                    <div class=" g-owlcarousel-layout-testimonial">
                                        <div id="g-owlcarousel-module-owlcarousel-504" class="g-owlcarousel owl-carousel g-owlcarousel-fullwidth owl-theme owl-loaded">
                                            <div class="owl-stage-outer">
                                                <div class="owl-stage" style="transform: translate3d(-5755px, 0px, 0px); transition: 0.25s; width: 8057px;">
                                                    <div class="owl-item cloned" style="width: 1151px; margin-right: 0px;">
                                                        <div class="g-owlcarousel-item-desc">
                                                            <i class="fa fa-thumbs-o-up"></i>      Дякую за допомогу. Все дуже зручно та комфортно. В одному закладі можна пройти всі обстеження і не треба годинами чекати в черзі! Особлива подяка лікарю Костянтину Анатолійовичу!
                                                        </div>
                                                    </div>
                                                    <div class="owl-item cloned" style="width: 1151px; margin-right: 0px;">
                                                        <div class="g-owlcarousel-item-desc">
                                                            <i class="fa fa-thumbs-o-up"></i>      Дякую за допомогу. Все дуже зручно та комфортно. В одному закладі можна пройти всі обстеження і не треба годинами чекати в черзі! Особлива подяка лікарю Костянтину Анатолійовичу!
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 1151px; margin-right: 0px;">
                                                        <div class="g-owlcarousel-item-desc">
                                                            <i class="fa fa-thumbs-o-up"></i>      Дякую за допомогу. Все дуже зручно та комфортно. В одному закладі можна пройти всі обстеження і не треба годинами чекати в черзі! Особлива подяка лікарю Костянтину Анатолійовичу!
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 1151px; margin-right: 0px;">
                                                        <div class="g-owlcarousel-item-desc">
                                                            <i class="fa fa-thumbs-o-up"></i>      Дякую за допомогу. Все дуже зручно та комфортно. В одному закладі можна пройти всі обстеження і не треба годинами чекати в черзі! Особлива подяка лікарю Костянтину Анатолійовичу!
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 1151px; margin-right: 0px;">
                                                        <div class="g-owlcarousel-item-desc">
                                                            <i class="fa fa-thumbs-o-up"></i>      Дякую за допомогу. Все дуже зручно та комфортно. В одному закладі можна пройти всі обстеження і не треба годинами чекати в черзі! Особлива подяка лікарю Костянтину Анатолійовичу!
                                                        </div>
                                                    </div>
                                                    <div class="owl-item cloned active" style="width: 1151px; margin-right: 0px;">
                                                        <div class="g-owlcarousel-item-desc">
                                                            <i class="fa fa-thumbs-o-up"></i>      Дякую за допомогу. Все дуже зручно та комфортно. В одному закладі можна пройти всі обстеження і не треба годинами чекати в черзі! Особлива подяка лікарю Костянтину Анатолійовичу!
                                                        </div>
                                                    </div>
                                                    <div class="owl-item cloned" style="width: 1151px; margin-right: 0px;">
                                                        <div class="g-owlcarousel-item-desc">
                                                            <i class="fa fa-thumbs-o-up"></i>      Дякую за допомогу. Все дуже зручно та комфортно. В одному закладі можна пройти всі обстеження і не треба годинами чекати в черзі! Особлива подяка лікарю Костянтину Анатолійовичу!
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="owl-controls">
                                                <div class="owl-nav">
                                                    <div class="owl-prev" style="display: none;">prev</div>
                                                    <div class="owl-next" style="display: none;">next</div>
                                                </div>
                                                <div class="owl-dots" style="">
                                                    <div class="owl-dot active"><span></span></div>
                                                    <div class="owl-dot"><span></span></div>
                                                    <div class="owl-dot"><span></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <section id="g-copyright" class="g-mobile-center-compact">
                <div class="g-container">
                    <div class="g-grid">
                        <div class="g-block size-60">
                            <div class="g-content">
                                <a href="" title="VinOsteo" rel="home" class="g-footer-logo">
                                    VinOsteo
                                </a>
                            </div>
                        </div>
                        <div class="g-block size-40">
                            <div class="g-content">
                                <div class="">
                                    <div id="g-newsletter-newsletter-8013" class="g-newsletter g-newsletter-fullwidth g-newsletter-aside-compact g-newsletter-square">
                                        <form id="g-newsletter-form-newsletter-8013" class="g-newsletter-form" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open( & #39; //feedburner.google.com/fb/a/mailverify?uri=', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
                                            <div class="g-newsletter-sidetext">Newsletter</div>
                                            <div class="g-newsletter-form-wrapper">
                                                <div class="g-newsletter-inputbox">
                                                    <input type="text" placeholder="" name="email">
                                                    <input type="hidden" value="" name="uri">
                                                    <input type="hidden" name="loc" value="en_US">
                                                </div>
                                                <div class="g-newsletter-button">
                                                    <a class="g-newsletter-button-submit " href="#" onclick="document.getElementById( & #39; g - newsletter - form - newsletter - 8013 & #39; ).submit()">
                                                        <span class="g-newsletter-button-icon"><i class="fa fa-send"></i></span>
                                                        <span class="g-newsletter-button-text"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="g-grid">
                        <div class="g-block g-copyright size-48">
                            <div class="g-content">
                                Copyright ©
                                2015 - 2016
                                Розробка та підтримка ресурсу Indever
                            </div>
                        </div>
                        <div class="g-block nomarginleft nopaddingleft size-52">
                            <div class="g-content">
                                <div class="moduletable ">
                                    <div class="g-grid g-sample-sitemap">
                                        <div class="g-block">
                                            <ul class="nomarginall g-bottom-menu align-right">
                                                <li><a href="index.php?58376d9d">Головна</a> </li>
                                                <li><a href="#"><i class="fa fa-circle"></i> Послуги</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i> Прайс</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i> Галерея</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i> Контакти</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript" src="js2/main.js"></script>
        <script type="text/javascript">
                                           jQuery(window).load(function(){
                                           jQuery('#module-newsslider-498 .news-content:first-child()').addClass('top-content');
                                           jQuery('#module-newsslider-498 .nh-anim:first-child()').addClass('selected first');
                                           jQuery('#module-newsslider-498 .g-newsslider-pagination li:first-child()').addClass('selected');
                                           });
                                           (function ($) {
                                           "use strict";
                                           var hl,
                                                   newsList = $('#module-newsslider-498 .news-headlines'),
                                                   navigation_next = $('#module-newsslider-498 .g-newsslider-navigation .next'),
                                                   navigation_prev = $('#module-newsslider-498 .g-newsslider-navigation .prev'),
                                                   pagination = $('#module-newsslider-498 .g-newsslider-pagination li'),
                                                   newsListItems = $('#module-newsslider-498 .news-headlines li'),
                                                   firstNewsItem = $('#module-newsslider-498 .news-headlines li:nth-child(1)'),
                                                   newsPreview = $('#module-newsslider-498 .news-preview'),
                                                   elCount = $('#module-newsslider-498 ul.news-headlines').children().length - 1,
                                                   vPadding = (parseInt(firstNewsItem.css('padding-top').replace('px', ''), 10)) +
                                                   (parseInt(firstNewsItem.css('padding-bottom').replace('px', ''), 10)),
                                                   vMargin = (parseInt(firstNewsItem.css('margin-top').replace('px', ''), 10)) +
                                                   (parseInt(firstNewsItem.css('margin-bottom').replace('px', ''), 10)),
                                                   cPadding = (parseInt($('.news-content').css('padding-top').replace('px', ''), 10)) +
                                                   (parseInt($('.news-content').css('padding-bottom').replace('px', ''), 10)),
                                                   speed = 5000, // this is the speed of the switch
                                                   myTimer = null,
                                                   siblings = null,
                                                   totalHeight = null,
                                                   indexEl = 0,
                                                   i = null;
                                           newsListItems.addClass('nh-anim');
                                           function doTimedSwitch() {
                                           myTimer = setInterval(function () {
                                           if (($('#module-newsslider-498 .selected').prev().index() + 1) === elCount) {
                                           firstNewsItem.trigger('click');
                                           } else {
                                           $('#module-newsslider-498 .selected').next(':not(.first)').trigger('click');
                                           }
                                           }, speed);
                                           }

                                           clearInterval(myTimer);
                                           doTimedSwitch();
                                           function doClickItem() {
                                           navigation_next.on('click', function () {
                                           $('#module-newsslider-498 .g-newsslider-headlines li.selected').next().trigger('click');
                                           });
                                           navigation_prev.on('click', function () {
                                           $('#module-newsslider-498 .g-newsslider-headlines li.selected').prev().trigger('click');
                                           });
                                           newsListItems.on('click', function () {
                                           newsListItems.removeClass('selected');
                                           $(this).addClass('selected');
                                           siblings = $(this).prevAll();
                                           totalHeight = 0;
                                           // this loop calculates the height of individual elements, including margins/padding
                                           for (i = 0; i < siblings.length; i += 1) {
                                           totalHeight += $(siblings[i]).height();
                                           totalHeight += vPadding;
                                           totalHeight += vMargin;
                                           }

                                           indexEl = $(this).index() + 1;
                                           $('#module-newsslider-498 .g-newsslider-pagination li').removeClass('selected');
                                           $('#module-newsslider-498 .g-newsslider-pagination li:nth-child(' + indexEl + ')').addClass('selected');
                                           $('#module-newsslider-498 .news-content:nth-child(' + indexEl + ')').siblings().removeClass('top-content');
                                           $('#module-newsslider-498 .news-content:nth-child(' + indexEl + ')').addClass('top-content');
                                           clearInterval(myTimer);
                                           // comment out the line below if you don't
                                           // want it to rotate automatically
                                           doTimedSwitch();
                                           });
                                           pagination.on('click', function () {

                                           pagination.removeClass('selected');
                                           $(this).addClass('selected');
                                           siblings = $(this).prevAll();
                                           totalHeight = 0;
                                           // this loop calculates the height of individual elements, including margins/padding
                                           for (i = 0; i < siblings.length; i += 1) {
                                           totalHeight += $(siblings[i]).height();
                                           totalHeight += vPadding;
                                           totalHeight += vMargin;
                                           }

                                           indexEl = $(this).index() + 1;
                                           $('#module-newsslider-498 .g-newsslider-headlines .nh-anim').removeClass('selected');
                                           $('#module-newsslider-498 .g-newsslider-headlines .nh-anim:nth-child(' + indexEl + ')').addClass('selected');
                                           $('#module-newsslider-498 .news-content:nth-child(' + indexEl + ')').siblings().removeClass('top-content');
                                           $('#module-newsslider-498 .news-content:nth-child(' + indexEl + ')').addClass('top-content');
                                           clearInterval(myTimer);
                                           // comment out the line below if you don't
                                           // want it to rotate automatically
                                           doTimedSwitch();
                                           });
                                           }

                                           function doWindowResize() {
                                           $(window).resize(function () {
                                           clearInterval(myTimer);
                                           $('#module-newsslider-498 .selected').trigger('click');
                                           });
                                           }

                                           // this is the poor man's 'init' section
                                           doClickItem();
                                           doWindowResize();
                                           $('#module-newsslider-498 .selected').trigger('click');
                                           })(jQuery);
        </script>
        <script type="text/javascript">window.getSize = function() { return {x: window.innerWidth, y: window.innerHeight } };</script>
        <script type="text/javascript">
          (function($) {
          function openFirstPanel(){
          jQuery('#module-accordion-467.accordion > li:first-child div').addClass('active').css('opacity', 0).slideDown("slow").animate({ opacity: 1 }, { queue: false, duration: 'slow' });
          jQuery('#module-accordion-467.accordion > li:first-child').addClass('active').slideDown("slow").animate({ opacity: 1 }, { queue: false, duration: 'slow' });
          jQuery('#module-accordion-467.accordion > li:first-child .toggle i').removeClass('fa-plus');
          jQuery('#module-accordion-467.accordion > li:first-child .toggle i').addClass('fa-minus');
          }

          var allPanels = jQuery('#module-accordion-467.accordion li .accordion-item-content').hide();
          openFirstPanel();
          jQuery('#module-accordion-467.accordion > li').click(function() {
          $this = jQuery(this);
          $target = jQuery('.accordion-item-content', this);
          if ($target.hasClass('active')){
          $target.removeClass('active').slideUp("slow").animate({ opacity: 0 }, { queue: false, duration: 'slow' });
          } else{
          allPanels.removeClass('active').slideUp("slow").animate({ opacity: 0 }, { queue: false, duration: 'slow' });
          $target.addClass('active').slideDown("slow").animate({ opacity: 1 }, { queue: false, duration: 'slow' });
          }
          if ($this.hasClass('active')){
          $this.removeClass('active');
          jQuery('.toggle i', this).removeClass('fa-minus');
          jQuery('.toggle i', this).addClass('fa-plus');
          } else{
          jQuery('#module-accordion-467.accordion li').removeClass('active');
          jQuery('#module-accordion-467 .toggle i').removeClass('fa-minus');
          jQuery('#module-accordion-467 .toggle i').addClass('fa-plus');
          $this.addClass('active');
          jQuery('.toggle i', this).removeClass('fa-plus');
          jQuery('.toggle i', this).addClass('fa-minus');
          }
          return false;
          });
          })(jQuery);
        </script>
        <script type="text/javascript">
          jQuery(window).load(function(){
          jQuery('#g-contenttabs-module-contenttabs-506').tabs({
          show: {
          effect: 'slide',
                  direction: 'left',
                  duration: 500
          }
          });
          });
        </script>
        <script type="text/javascript" src="js2/owlcarousel.js"></script>
        <script type="text/javascript">
          jQuery(window).load(function(){
          jQuery('#g-owlcarousel-module-owlcarousel-507').owlCarousel({
          items: 1,
                  rtl: false,
                  nav: true,
                  navText: ['\x3Ci\x20class\x3D\x22fa\x20fa\x2Dchevron\x2Dleft\x22\x3E\x3C\x2Fi\x3E', '\x3Ci\x20class\x3D\x22fa\x20fa\x2Dchevron\x2Dright\x22\x3E\x3C\x2Fi\x3E'],
                  dots: false,
                  loop: true,
                  autoplay: true,
                  autoplayTimeout: 5000,
                  autoplayHoverPause: 1,
          })
          });
        </script>
        <script type="text/javascript">
          jQuery(window).load(function(){
          jQuery('#g-owlcarousel-module-owlcarousel-504').owlCarousel({
          items: 1,
                  rtl: false,
                  nav: false,
                  dots: true,
                  loop: true,
                  autoplay: true,
                  autoplayTimeout: 5000,
                  autoplayHoverPause: 1,
          })
          });
        </script>
        <div data-rokboxwrapper="true" class="rokbox-wrapper">
            <div data-rokboxouter="true" class="rokbox-outer">
                <div data-rokboxheader="true" class="rokbox-header">
                    <div data-rokboxicon="x" data-rokboxclose="true" class="rokbox-close"></div>
                </div>
                <div data-rokboxrow="true" class="rokbox-row">
                    <div data-rokboxinner="true" class="rokbox-inner">
                        <div data-rokboxcontainer="true" class="rokbox-container">
                            <div data-rokboxloader="true" class="rokbox-loader">
                                <div class="rokbox-loader-image"></div>
                            </div>
                            <div data-rokboxcontent="true" class="rokbox-content"></div>
                            <div data-rokboxcontrols="true" class="rokbox-controls">
                                <div data-rokboxicon="x" data-rokboxclose="true" class="rokbox-close"></div>
                                <div data-rokboxprevious="true" class="rokbox-previous">
                                    <div data-rokboxicon="p"></div>
                                </div>
                                <div data-rokboxnext="true" class="rokbox-next">
                                    <div data-rokboxicon="n"></div>
                                </div>
                                <div data-rokboxicon="d" data-rokboxfitscreen="true" class="rokbox-fitscreen"></div>
                                <div data-rokboxicon="w" data-rokboxunfitscreen="true" class="rokbox-unfitscreen"></div>
                            </div>
                            <div data-rokboxcaption="true" class="rokbox-caption"></div>
                        </div>
                    </div>
                </div>
                <div data-rokboxfooter="true" class="rokbox-footer">
                    <div data-rokboxprevious="true" class="rokbox-previous">
                        <div data-rokboxicon="p"></div>
                    </div>
                    <div data-rokboxnext="true" class="rokbox-next">
                        <div data-rokboxicon="n"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>