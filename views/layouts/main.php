<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use app\assets\AppAsset;

AppAsset::register($this);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => yii\helpers\Url::to('/img/logo.png')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<meta name="viewport" content="width=450, initial-scale=1.0">-->

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-90402742-1', 'auto');
          ga('send', 'pageview');

        </script>
        <!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter42148384 = new Ya.Metrika({
                    id:42148384,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<!-- /Yandex.Metrika counter -->
    </head>
    <body>
        
    <noscript><div><img src="https://mc.yandex.ru/watch/42148384&quot; style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
        
        
        <?php $this->beginBody() ?>
        <!-- Wrap all page content here -->
        <div id="wrap" class="" style="background: #eee; ">
            <div id="header" class="navbar navbar-custom navbar-static-top green-gradient_" id="nav">
                <div id="header-top" class="" style="position: relative; /*background: #fff*/">
                    <div class="container">
                        <ul class="nav header-top" style="">
                            <li style="/*margin-right: 15px;float: left;*/">
                                <i class="fa fa-phone-square"></i> 
                                <div class="info" style="display:inline-block;">
                                    <div>(0432) 655-044 </div>
                                    <div>(068) 288-44-88</div>
                                    <div>(063) 525-58-55</div>
                                </div>
                            </li>

                            <li style="/*margin-right: 15px;float: left;*/">
                                <i class="fa fa-clock-o"></i> 
                                <div class="info" style="display:inline-block;">
                                    <div>Понедельник - Пятница 09:00-19:00</div>
                                    <div>СУББОТА - по предварительной записи</div>
                                    <div>ВОСКРЕСЕНЬЕ - ВЫХОДНОЙ</div>
                                </div>
                            </li>

                            <li style="/*margin-right: 15px;float: left;*/">
                                <i class="fa fa-map-marker"></i> 
                                <div class="info" style="display:inline-block;">
                                    <div>г.Винница</div>
                                    <div>ул. Киевская, 104</div>
                                </div>
                            </li>

                            <li class="social-icons" style="/*margin-left: 55px; margin-right: 15px;float: left; */">
                                <a target="_blank" href="https://www.facebook.com/%D0%9E%D1%81%D1%82%D0%B5%D0%BE%D0%BF%D0%B0%D1%82%D0%B8%D1%87%D0%BD%D0%B8%D0%B9-%D1%86%D0%B5%D0%BD%D1%82%D1%80-363975590647854/" style="padding: 0px; display: inline-block; vertical-align: top; "><i class="fa fa-facebook-square" style="font-size: 30px;  "></i> </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="fixed-panel" class="" style="background: rgba(45, 133, 54, 1);">
                    <div class="fon-green">
                    </div>
                    <div class="container">
                        <div class="row" style="position: relative;">
                            <div class="navbar-header text-center">
                                <div class="mob mob-title-company">
                                    <a style="color:#fff;"><span>Остеопатический центр</span></a>
                                </div>
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                            </div>
                            <div class="logo-body-block larg" style="">
                                <a href="<?= Url::home() ?>"><img class="logo-img-header" src="<?= Url::to('/img/logo.png') ?>" style="height: 100%; "></a>
                            </div>
                            <div class="collapse navbar-collapse  " >

                                <ul class="nav navbar-nav navbar-left header-list" style="">
                                    <li class="dropdown menu-item-has-children">
                                        <a href="<?= Url::to("/what") ?>" class=" <?= (Yii::$app->controller->action->id == 'what' ? 'current' : '') ?> " >
                                            <span class="g-menu-item-title">Что и как  мы лечим?</span>
                                        </a>

                                    </li>
                                    <li class="dropdown menu-item-has-children">
                                        <a href="#" class="dropdown-toggle  <?= (Yii::$app->controller->action->id == 'osteopathicsession' ? 'current' : '') ?>  <?= (Yii::$app->controller->action->id == 'osteopathywhat' ? 'current' : '') ?>  <?= (Yii::$app->controller->action->id == 'whyosteopathy' ? 'current' : '') ?>  <?= (Yii::$app->controller->action->id == 'contraindications' ? 'current' : '') ?>" data-toggle="dropdown">
                                            <span class="g-menu-item-title">Остеопатия</span>
                                        </a>
                                        <ul class="dropdown-menu " style="">
                                            <li><a href="<?= Url::to("/osteopathicsession", true) ?>" class="<?= (Yii::$app->controller->action->id == 'osteopathicsession' ? 'active' : '') ?> ">Остеопатический сеанс</a></li>
                                            <li><a href="<?= Url::to("/osteopathywhat", true) ?>" class="<?= (Yii::$app->controller->action->id == 'osteopathywhat' ? 'active' : '') ?> ">Остеопатия: что это?</a></li>
                                            <li><a href="<?= Url::to("/whyosteopathy", true) ?>" class="<?= (Yii::$app->controller->action->id == 'whyosteopathy' ? 'active' : '') ?> ">Почему остеопатия?</a></li>
                                            <li><a href="<?= Url::to("/contraindications", true) ?>" class="<?= (Yii::$app->controller->action->id == 'contraindications' ? 'active' : '') ?> ">Противопоказания</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown menu-item-has-children">
                                        <a href="#" class="dropdown-toggle <?= (Yii::$app->controller->action->id == 'kinesitherapy' ? 'current' : '') ?> <?= (Yii::$app->controller->action->id == 'massagetechniques' ? 'current' : '') ?> <?= (Yii::$app->controller->action->id == 'yogatherapy' ? 'current' : '') ?>" data-toggle="dropdown">
                                            <span class="g-menu-item-title">Дополнительные  методы лечения</span>
                                        </a>
                                        <ul class="dropdown-menu " style="">
                                            <li><a href="<?= Url::to("/kinesitherapy", true) ?>" class="<?= (Yii::$app->controller->action->id == 'kinesitherapy' ? 'active' : '') ?> ">Кинезитерапия</a></li>
                                            <li><a href="<?= Url::to("/site/massagetechniques", true) ?>" class="<?= (Yii::$app->controller->action->id == 'massagetechniques' ? 'active' : '') ?> ">Массажные техники</a></li>
                                            <li><a href="<?= Url::to("/site/yogatherapy", true) ?>" class="<?= (Yii::$app->controller->action->id == 'yogatherapy' ? 'active' : '') ?> ">Йога-терапия</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown menu-item-has-children">
                                        <a href="#" class="dropdown-toggle <?= (Yii::$app->controller->action->id == 'sporting-features-body-and-sports-injury' ? 'current' : '') ?>  <?= (Yii::$app->controller->action->id == 'what-athlete-osteopathic-correction' ? 'current' : '') ?>  <?= (Yii::$app->controller->action->id == 'kinesitherapy-for-sportsmens' ? 'current' : '') ?> " data-toggle="dropdown">
                                            <span class="g-menu-item-title">Для спортсменов</span>
                                        </a>
                                        <ul class="dropdown-menu " style="">
                                            <li><a href="<?= Url::to("/site/sporting-features-body-and-sports-injury", true) ?>" class="<?= (Yii::$app->controller->action->id == 'sporting-features-body-and-sports-injury' ? 'active' : '') ?> ">«Спортивные» особенности тела и спортивная травма</a></li>
                                            <li><a href="<?= Url::to("/site/what-athlete-osteopathic-correction", true) ?>" class="<?= (Yii::$app->controller->action->id == 'what-athlete-osteopathic-correction' ? 'active' : '') ?> ">Зачем спортсмену остеопатическая коррекция?</a></li>
                                            <li><a href="<?= Url::to("/site/kinesitherapy-for-sportsmens", true) ?>" class="<?= (Yii::$app->controller->action->id == 'kinesitherapy-for-sportsmens' ? 'active' : '') ?> ">Кинезитерапия для спортсменов</a></li>
                                        </ul>
                                    </li>
                                    <li class="">
                                        <a href="<?= Url::to('/site/read-important', true) ?>" class="<?= (Yii::$app->controller->action->id == 'read-important' ? 'current' : '') ?>" ><span class="g-menu-item-title">Перед приёмом прочитайте!</span></a>
                                    </li>
                                    <li class="">
                                        <a href="<?= Url::to('/site/price', true) ?>" class="<?= (Yii::$app->controller->action->id == 'price' ? 'current' : '') ?>" ><span class="g-menu-item-title">Прайс</span></a>
                                    </li>
                                    <li class="">
                                        <a href="<?= Url::to('/contacts', true) ?>" class="<?= (Yii::$app->controller->action->id == 'contacts' ? 'current' : '') ?>" ><span class="g-menu-item-title">Контакти</span></a>
                                    </li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div><!--/.container -->
                    </div><!--/.container -->
                </div><!--/.container -->
            </div><!--/.navbar -->





            <?php
            echo $content;
            ?>
        </div><!--/wrap-->

        <!--        <div id="footer">
                    <div class="container">
                        <p class="text-muted">Cайт <a href="<?php echo yii\helpers\Url::home() ?>"> <?= Yii::$app->request->serverName; ?></a></p>
                    </div>
                </div>-->

        <?php $this->endBody() ?>

        <!--<script type='text/javascript' src="http://maps.googleapis.com/maps/api/js?sensor=false&extension=.js&output=embed"></script>-->


        <!-- JavaScript jQuery code from Bootply.com editor  -->
        <script type='text/javascript'>
          $(document).ready(function () {
              /* affix the navbar after scroll below header */
//              $('#nav').affix({
//                  offset: {
//                      top: $('header').height() - $('#nav').height()
//                  }
//              });

              /* highlight the top nav as scrolling occurs */
//              $('body').scrollspy({target: '#nav'})
          });

        </script>

        <!-- Chatra {literal} -->
        <script>
          (function (d, w, c) {
              w.ChatraID = 'h4FcbEfnLeGZRGMLc';
              var s = d.createElement('script');
              w[c] = w[c] || function () {
                  (w[c].q = w[c].q || []).push(arguments);
              };
              s.async = true;
              s.src = (d.location.protocol === 'https:' ? 'https:' : 'http:')
                      + '//call.chatra.io/chatra.js';
              if (d.head)
                  d.head.appendChild(s);
          })(document, window, 'Chatra');
        </script>
         <!--/Chatra {/literal}--> 
    </body>
</html>
<?php $this->endPage() ?>
