<?php

use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Йога-терапия | Остеопатический центр';


$description = "Йога-терапия – адаптированные для ослабленного болезнью организма движения, без опасных и провоцирующих движений.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
    'url' => Yii::$app->request->absoluteUrl,
    'description' => $description,
    'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);
?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">
            <div class="col-md-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Йога-терапия</h2>
            </div>

            <div class="col-md-12" style="margin-top: 30px;">
                <p>
                    Йога-терапия – <b>адаптированные </b>для ослабленного болезнью организма движения, <b>без опасных и провоцирующих движений.</b>
                </p>
            </div>



            <div class="col-md-12" style="margin-top: 30px;">
                <div class="col-md-4">
                    <img src="<?= Url::to('/images3/иога.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>
                <div class="col-md-8">
                    <p>
                        Отличие Йога-терапии в ее <b>осторожности,бережности к позвоночнику и суставам</b>.Занятия включают простые упражнения,
                        задача которых — выровнять тело, вытянуть позвоночник, вернуть на место внутренние органы, развить способность расслабляться и
                        удерживать баланс. <b>Один из важных аспектов йога-терапии — развитие внимательности к телу</b>, к процессам, в нем протекающим. Это позволяет не только
                        понять причины своих проблем, но и устранить их. Но не стоит думать, что вернуть здоровье можно одними упражнениями. Они не имеют ни смысла, ни
                        действия в отрыве от тренировки духа.
                    </p>
                </div>
            </div>

            <div class="col-md-12" style="margin-top: 30px;">
                <div class="col-md-8">
                    <ul class="li-style" style="padding-left: 0px;">
                        <li>1. Физический аспект - <b>укрепление мышц, связок, развитие гибкости и устойчивости.</b></li>
                        <li>2. Физиологический аспект - <b>улучшение</b> кровообращения и микроциркуляции, регуляция эндокринной , иммунной системы, улучшение ферментативных процессов.</li>
                        <li>3. <b>Психо-эмоциональный аспект </b> -<b> выравнивание</b> потенциалов мозга, выделение в кровь нейромедиаторов (бэта эндорфинов, дофамина и т.д.), улучшение памяти и воли благодаря концентрации внимания.</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <img src="<?= Url::to('/images3/иога 2.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>
            </div>
        </div>
    </div>
</section>
