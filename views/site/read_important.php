<?php

use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Перед приёмом прочитайте! | Остеопатический центр';


$description = "Наша цель - вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать это состояние.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url' => Yii::$app->request->absoluteUrl,
    'description' => $description,
    'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);
?>
<section class="container features section-top" >
    <div class="row">
        <div class="col-lg-12 wtite background" style="padding-top:65px;padding-bottom: 25px;">

            <!--<h1> Перед приёмом прочитайте!</h1>-->
            <!--<h2>Остеопатия для спортсменов решает проблемы:</h2>-->

            <p>
                Врач-остеопат проводит детальное обследование вашего тела «от головы до пяток»,

                анализирует положение определенных частей тела относительно друг друга, осанку,

                положение тела стоя, во время ходьбы, сидя и лѐжа, поэтому Вас попросят раздеться до

                нижнего белья. 
            </p>
            <div class="col-md-12 col-xs-12 text-center block-with-image">
                <img src="/img/doc.jpg" class="" style= "width: 100%; max-width: 250px; float: none; border-radius: 6px; padding: 0px;">
            </div>
            <div class="clearfix"></div>
            <div style="margin-top:25px; margin-bottom: 10px;">
                <b>Удобной формой одежды на приёме для Вас будет:</b>
            </div>
            <ul>
                <li style="list-style-type: none; "> <img src= "/img/man.jpg" style="height: 50px; width: 40px">

                    для мужчин – <span style="font-style: italic;">удобное нижнее бельё, которое тянется;</span
                </li>
                <p></p>
                <li style="list-style-type: none; "> <img src= "/img/woman.jpg" style="height: 50px; width: 40px">
                    для женщин – <span style="font-style: italic;">удобный бюстгалтер, шорты, рейтузы или лосины, которые тянутся.</span>
                </li>
            </ul>


            <div style="margin-top:25px;">
                Перед приемом рекомендуется принять душ и выполнить другие гигиенические процедуры.
            </div>

            <div style="margin-top:25px;">
                <b>С собой взять:</b> данные предыдущих исследований (рентгеновские снимки, снимки МРТ, КТ, данные лабораторных методов исследования, врачебные заключения, выписки из стационара
                и пр.).
            </div>


            <div style="margin-top:25px;">
                На приѐм рекомендуется прийти на 10 минут раньше, чтобы успеть переодеться,
                ознакомиться и заполнить необходимую документацию.
            </div>

            <div style="margin-top:25px;">
                <div class="col-md-3 col-xs-12">
                    <img src="/images3/спорт 8.jpg" class="col-md-9 col-xs-12"  style=" border-radius: 6px; padding: 0px;">
                </div>
                <div class="col-md-9 col-xs-12" style="">
                    <p>
                        В течение 3-4 дней после остеопатического лечения следует
                        избегать больших физических нагрузок и резких движений.
                    </p>
                    <div style="margin-top:25px;">
                        <b>Удобная для вас форма одежды при посещении сеанса

                            кинезитерапии или йога-терапии:</b> удобная для Вас

                        спортивная одежда, которая тянется. Рекомендуем иметь при

                        себе небольшое полотенце для лица.
                    </div>

                    <div style="margin-top:25px;">
                        В центре предусмотрена комната для переодевания и отдыха.
                    </div>
                    <div style="margin-top:25px;">
                        В своей работе мы строго соблюдаем принципы врачебной

                        этики и врачебной тайны.
                    </div>

                </div>
            </div>





        </div>

        <br />
        <br />
    </div>
</section>
