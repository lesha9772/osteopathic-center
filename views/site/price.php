<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Прайс | Остеопатический центр';

$description = "Наша цель - вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать это состояние.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url'=>Yii::$app->request->absoluteUrl,
    'description' => $description,
    'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);

?>

<section class="container features section-top">
    <div class="row">
        <div class="col-lg-12 wtite background" style="padding-top:65px;padding-bottom: 25px;">

            <div class="">
                <h2>ПЕРЕЧЕНЬ И СТОИМОСТЬ УСЛУГ</h2>
                <!--<p>The .table-hover class enables a hover state on table rows:</p>-->            
                <table class="table table-hover" >
                    <thead>
                        <tr>
                            <th>№ п/п</th>
                            <th>Перечень услуг</th>
                            <th>Длительность</th>
                            <th>Стоимость услуг</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Сеанс остеопатии</td>
                            <td>50 мин.</td>
                            <td>200,00 грн.</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Массаж (спортивный, медицинский,оздоровительный)</td>
                            <td>30-50 мин.</td>
                            <td>150,00 грн</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Кинезитерапия (индивидуальное занятие с реабилитологом,индивидуальный подбор упражнений)</td>
                            <td>50 мин.</td>
                            <td>120,00 грн.</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Комплекс физиотерапивтичных (электротерапия, нейростимуляция,миостимуляция, светотерапия,ультразвук)</td>
                            <td>30 мин.</td>
                            <td>50,00 грн.</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Первичная консультация</td>
                            <td> </td>
                            <td>150,00 грн.</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Повторная консультация (коррекция лечения)</td>
                            <td> </td>
                            <td>50,00 грн.</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="">
                <h2>КОМПЛЕКСНАЯ РЕАБИЛИТАЦИЯ</h2>
                <!--<p>The .table-hover class enables a hover state on table rows:</p>-->            
                <table class="table table-hover" >
                    <thead>
                        <tr>
                            <th class="npp">№ п/п</th>
                            <th>Перечень услуг</th>
                            <th class="time">Длительность</th>
                            <th class="price">Стоимость услуг</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="npp">1</td>
                            <td>Сеанс оостеопатии + массаж оздоровительный или кинезитерапия + физиотерапия (по назначению врача)</td>
                            <td class="time">60-70 мин.</td>
                            <td  class="price">250,00 грн.</td>
                        </tr>
                        <tr>
                            <td class="npp">2</td>
                            <td>Кинезитерапия (индивидуальное занятие с реабилитологом) + массаж оздоровительный + физиотерапия (по назначению врача)</td>
                            <td class="time">60-70 мин.</td>
                            <td class="price">200,00 грн</td>
                        </tr>
                        <tr>
                            <td class="npp">3</td>
                            <td> Сеанс остеопатии+ кинезитерапия (индивидуальное занятие с реабилитологом) + массаж оздоровительный + физиотерапия (по назначению врача) </td>
                            <td class="time">90-100 мин.</td>
                            <td class="price">300,00 грн.</td>
                        </tr>

                    </tbody>
                </table>
            </div>


            <div class="">
                <h2>ПЕРЕЧЕНЬ И СТОИМОСТЬ УСЛУГ МАССАЖА</h2>
                <!--<p>The .table-hover class enables a hover state on table rows:</p>-->            
                <table class="table table-hover" >
                    <thead>
                        <tr>
                            <th class="npp">№ п/п</th>
                            <th>Перечень услуг</th>
                            <th class="time">Длительность</th>
                            <th class="price">Стоимость услуг</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="npp">1</td>
                            <td>Массаж спины</td>
                            <td class="time">35 -50 мин.</td>
                            <td  class="price">150,00 грн.</td>
                        </tr>
                        <tr>
                            <td class="npp">2</td>
                            <td>Массаж спины, грудной клетки и живота</td>
                            <td class="time">60 мин.</td>
                            <td class="price">200,00 грн</td>
                        </tr>
                        <tr>
                            <td class="npp">3</td>
                            <td>Массаж шейно-воротниковой зоны </td>
                            <td class="time">20 мин.</td>
                            <td class="price">120,00 грн.</td>
                        </tr>
                        <tr>
                            <td class="npp">3</td>
                            <td>Массаж нижних конечностей </td>
                            <td class="time">30-45 мин.</td>
                            <td class="price">150,00 грн.</td>
                        </tr>
                        <tr>
                            <td class="npp">3</td>
                            <td>Массаж верхних конечностей </td>
                            <td class="time">20 мин.</td>
                            <td class="price">120,00 грн.</td>
                        </tr>
                    <td class="npp">3</td>
                    <td>Общий массаж </td>
                    <td class="time">20 мин.</td>
                    <td class="price">120,00 грн.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

</section>


