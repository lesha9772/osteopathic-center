<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use \yii\timeago\TimeAgo;
use yii\bootstrap\Alert;
use yii\widgets\Pjax;

$this->title = 'Отзывы';
?>
<style>
    .time-ago-comment {
        color: #8b9da7;
        line-height: 16px;
        margin-right: 12px;
    }

    .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #2d8536;
        border-color: #216428;
    }

    .pagination > li > a, .pagination > li > span {
        color: #369a40;
    }


    .media:first-child {
        margin-top: 30px;
    }
    .media{
        margin-top: 40px;
    }
    .p-feedbacksList > li {
        border-bottom: 0 !important;
        padding-bottom: 0;
    }

    .f-infoText {
        padding: 15px;
        background: #eeeeee;
        margin-top: 15px;
        margin-right: 17px;
        position: relative;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        color: #333333;
        font-size: 13px;
        margin-bottom: 15px;
        margin-left: 74px;
    }

    .p-infoText {
        margin-left: 0 !important;
        margin-bottom: 0;
        margin-top: 5px;
    }


    .media-list{
        margin-bottom: 25px;
    }
</style>
<?php
Pjax::begin();
?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div id="content-block" class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">

            <div class="col-md-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Оставьте Ваш отзыв</h2>
            </div>

            <div class="col-md-offset-2 col-md-8 col-xs-12 " style="margin-top: 10px;">
                <?php
                if (Yii::$app->session->hasFlash('successfull')) {
                  echo Alert::widget([
                      'options' => [
                          'class' => 'alert-success'
                      ],
                      'body' => 'Спасибо за Ваш отзыв. Нам важно знать Ваше мнение.'
                  ]);
                }
                ?>

                <div class="col-lg-12 " style="margin-bottom: 70px; border-bottom: 2px solid #2d8536; padding-bottom: 45px; ">
                    <?php
                    $form = ActiveForm::begin([
//                            'validationUrl' => ['validation'],
//                            'enableAjaxValidation' => true,
//                            'enableClientValidation' => false,
                                'method' => 'post',
//                            'action' => ['callbackercall'],
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'validateOnType' => true,
                                'options' => ['class' => 'form-horizontal',
                                    'enctype' => 'multipart/form-data'
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\"></div>",
                                    'labelOptions' => ['class' => 'col-md-12 label-group'],
                                    'inputOptions' => ['class' => 'form-control'],
                                ],
                    ]);
                    ?>



                    <div class="col-md-2">
                        <img src="/img/comment.png" class="img-circle" alt="">
                    </div>
                    <div class="col-md-10">
                        <?= $form->field($model, 'name')->textInput(['type' => 'text', 'placeholder' => 'Имя'])->label(false) ?>
                    </div>

                    <div class="clearfix"></div>



                    <div class="col-md-2">
                    </div>
                    <div class="col-md-10">
                        <?= $form->field($model, 'text')->textarea(['placeholder' => 'Оставьте Ваш отзыв', 'style' => 'margin-top:0px; resize: vertical; width:100%;'])->label(false) ?>
                    </div>


                    <div class="clearfix"></div>


                    <div class="col-md-offset-2 col-md-10">
                        <button type="submit" class="btn btn-success" style="width: 100%;">Оставить отзыв</button>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>



                <?php if ($comments_list): ?>

                  <?php foreach ($comments_list as $oneComment): ?>
                    <div class="col-md-2">
                        <img src="/img/comment.png" class="img-circle" alt="">
                    </div>
                    <div class="col-md-10 o-freelancersList p-feedbacksList media-list">
                        <p class="comments-username"><b><?= $oneComment->name ?> </b></p>
                        <p class="time-ago-comment">отзыв добавлен <?= TimeAgo::widget(['timestamp' => $oneComment->created_at, 'language' => 'ru']); ?></p>
                        <div class="f-infoText p-infoText">

                            <div class="j-opinion">
                                <?= $oneComment->text ?>                                                    </div>


                        </div>
                    </div>
                    <div class="clearfix"></div>

                  <?php endforeach; ?>
                <?php endif; ?>






                <!--                <ul class="o-freelancersList p-feedbacksList media-list">
                                    <li class="media">
                                        <div style="float: left;
                                             margin-right: 10px;
                                             position: relative;">
                                            <img src="//freelance.ua/files/images/avatars/132/132348sf11188.jpeg" class="img-circle" alt="Евгений Киршенин">
                                        </div>
                                        <div class="" style="float: left;">
                                            <p class="comments-username"><b>Евгений Киршенин </b></p>
                                            <p>Оценка центра: 5</p>
                                            <p>отзыв добавлен 12.12.2016</p>
                
                                            <div class="f-infoText p-infoText">
                
                                                <div class="j-opinion">
                                                    Очень оперативно, выполнил поставленные задачи.                                                    </div>
                
                
                                            </div>
                                        </div>
                
                                    </li>
                                    <li class="media">
                                        <div style="float: left;
                                             margin-right: 10px;
                                             position: relative;">
                                            <img src="//freelance.ua/files/images/avatars/132/132348sf11188.jpeg" class="img-circle" alt="Евгений Киршенин">
                                        </div>
                                        <div class="" style="float: left;">
                                            <p class="comments-username"><b>Евгений Киршенин </b></p>
                                            <p>Оценка центра: 5</p>
                                            <p>отзыв добавлен 12.12.2016</p>
                
                                            <div class="f-infoText p-infoText">
                                                <div class="j-opinion">
                                                    Очень оперативно, выполнил поставленные задачи.                                                    </div>
                                            </div>
                                        </div>
                
                                    </li>
                
                
                                </ul>-->


            </div>


            <div class=" col-md-offset-2 col-md-8 col-xs-12 " style="">
                <?php
                // display pagination
                echo LinkPager::widget([
                    'pagination' => $pages,
                    'activePageCssClass' => 'active'
                ]);
                ?>
            </div>



        </div>

        <br />
        <br />
    </div>

</section>


<?php
Pjax::end();

$js = ' $(".pagination li").click(function () {
      $("html, body").animate({
          scrollTop: $("#header").offset().top
      }, 2000);
  });';


Yii::$app->view->registerJs($js);


