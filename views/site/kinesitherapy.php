<?php

use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Кинезитерапия | Остеопатический центр';

$description = "Кинезитерапия – лечение правильным движением по индивидуально разработанной программе на специальных тренажерах.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url' => Yii::$app->request->absoluteUrl,
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);
?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">

            <div class="col-md-12" style=" margin-top: 30px; ">
                <div class="col-md-4 col-xs-12 block-with-image" style="">
                    <img src="<?= Url::to('/images3/кинези.jpg') ?>" alt="Остеопатический центр" style="width: 100%; border-radius: 6px;" >
                </div>
                <div class="col-md-8 col-xs-12" style="margin-top: 100px;">
                    <p>
                        <b> Кинезитерапия </b>– лечение <b>правильным  движением </b>по индивидуально разработанной программе на <b>специальных тренажерах.</b>  
                    </p>
                </div>
            </div>



            <div class="col-md-12 col-xs-12" style=" margin-top: 30px; ">
                <h3 style="padding-top: 0px;"><b>Особенности кинезитреапии:</b></h3>
                <div class="col-md-3 col-xs-12 mob planshet" style="">
                    <img src="<?= Url::to('/images3/кинези 2.jpg') ?>" alt="Остеопатический центр" style="width: 100%; border-radius: 6px;" >
                </div>

                <div class="col-md-9 col-xs-12" style="">

                    <ul style="list-style-type: none;" class="mob-no-paddig-left">
                        <li>
                            - лечение <b>адаптированным  движением</b>, которое максимально <b>безопасно</b> и соответствует анамнезу, возрастным и другим физиологическим особенностям человека. 
                        </li>
                        <li>
                            - движение на специально разработанных <b>лечебных  тренажерах</b>;
                        </li>
                        <li>
                            - <b>точное дозирование</b> нагрузки на отдельные группы мышц;
                        </li>
                        <li>
                            - правильная <b>техника движения</b> и дыхания;
                        </li>
                        <li>
                            - <b>контроль</b> частоты сердечных сокращений и артериального давления; 
                        </li>
                        <li>
                            - контроль <b>инструктора </b>на протяжение всего сеанса кинезитерапии. 
                        </li>
                    </ul>

                    <p>
                        В среднем курс состоит из 10 занятий продолжительностью 1 час, при посещении 2 раза в неделю.
                    </p>

                </div>
                <div class="col-md-3 desctop" style="">
                    <img src="<?= Url::to('/images3/кинези 2.jpg') ?>" alt="Остеопатический центр" style="width: 100%; border-radius: 6px;" >
                </div>
            </div>






            <div class="col-md-12 col-xs-12" style=" margin-top: 30px; ">
                <h3 style="padding-top: 0px;"><b>Почему кинезитерапия?</b></h3>

                <div class="col-md-3 col-xs-12 block-with-image" style="">
                    <img src="<?= Url::to('/images3/кинези 3.jpg') ?>" alt="Остеопатический центр" style="width: 100%; border-radius: 6px;" >
                </div>

                <div class="col-md-9 col-xs-12" style="">

                    <p> 
                        1. Только кинезитерапия позволяет <b>укрепить мышечный корсет и создать правильный двигательный стереотип</b>, (в итоге создать равновесное положение тела). Также кинезитерапия позволяет закрепить достигнутый результат.
                    </p>
                    <p>
                        2. <b>Правильное мышечное и костное действие обеспечивает внутреннее здоровье организма и наоборот.</b>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12" style=" margin-top: 30px; ">


                <div class="col-xs-12 block-with-image mob planshet " style="">
                    <img src="<?= Url::to('/images3/кинези 4.jpg') ?>" alt="Остеопатический центр" style="width: 100%; border-radius: 6px;" >
                </div>

                <div class="col-md-9 col-xs-12" style="">

                    <p>
                        3. <b>Для того чтобы тело было здоровым, все наши мышцы должны сокращаться и расслабляться.</b> К сожалению, привычные бытовые занятия нагружают тело неравномерно: одни мышцы оказываются постоянно напряжены, и вследствие длительного спазма кровообращение в этой зоне нарушается, другие не задействуются вовсе и гипотрофируются. Только осознанно и сосредоточенно выполняя физические упражнения, можно дать возможность телу функционировать так, как это заложено в нем природой.
                    </p>
                </div>

                <div class="col-md-3 desctop" style="">
                    <img src="<?= Url::to('/images3/кинези 4.jpg') ?>" alt="Остеопатический центр" style="width: 100%; border-radius: 6px;" >
                </div>


            </div>
            <div class="col-md-12 col-xs-12" style=" margin-top: 30px; ">

                <div class="col-md-3 col-xs-12 block-with-image" style="">
                    <img src="<?= Url::to('/images3/кинези 5.jpg') ?>" alt="Остеопатический центр" style="width: 100%; border-radius: 6px;" >
                </div>

                <div class="col-md-9 col-xs-12" style="">

                    <p>
                        4. <b>Для того чтобы тело было здоровым, движения в суставах должны выполняться в полной амплитуде.</b>  Движения в суставах, как правило, происходят не в максимальном, а лишь в очень ограниченном диапазоне, и их работоспособность снижается. 
                    </p>

                    <p>
                        5. Кинезитерапия не только восстанавливает структуру тела,<b> но дарит уверенность в себе и эмоциональную стабильность</b>. Когда человек полностью сосредотачивается на своем теле, на правильном выполнении упражнений, он освобождается от отрицательных  эмоций. В то же время, любые физические занятия вызывают выработку эндорфинов — «гормонов радости».
                    </p>
                </div>
            </div>


        </div>

    </div>
</section>