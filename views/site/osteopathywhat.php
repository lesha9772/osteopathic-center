<?php
use app\components\widgets\AlexMetaTagsWidget;
$this->title = 'Остеопатия: что это? | Остеопатический центр';


$description = "Наша цель - вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать это состояние.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
    'url'=>Yii::$app->request->absoluteUrl,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);
?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">
            <div class="row">

                <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                    <h2 style="font-weight: 600;text-decoration: underline;">Остеопатия: что это?</h2>
                </div>


                <div class="col-md-12 col-xs-12" style="margin-top: 30px;">

                    <div class="col-md-4 col-xs-12 block-with-image">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQDxASEBIQDxUQEBYVEBYQEg8QEBUVFRUWFhUSFRUYHSggGBolHxUVITIhJSktLjAuGB8zODMtNygtLisBCgoKDg0OGxAQGy0lIB8tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSstLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIDBQYHBAj/xABDEAABAwICBgcEBwYFBQAAAAABAAIDBBEFIQYSMUFRYQcTInGBkaEUQlKxIzIzgpLB0RVicnOy8ENjouHxU4Oz0uL/xAAbAQEAAgMBAQAAAAAAAAAAAAAABAUBAgMGB//EAC8RAQACAQMDAgQGAQUAAAAAAAABAgMEESEFEjFBURMiYYEUMnGRobFCBhXB0fH/2gAMAwEAAhEDEQA/AO4oCAgICAgICAgi6Dw4ljNNTC880UN9mu9oJ7htK1tetfMu2LTZcs7Y6zP6Ncqek3DGX1ZJZbfBFJbzcAuM6rHHqsadD1lo37dv1l529KmHbxUD/tt/9lr+Lxun+waz2j93vpOkXC5Dbr+rP+bHIwfiIt6reNRjn1cMnRtZTmab/py2GhxKCdutDLHKOMb2vHousWifEq/JhyY52vWYem62c0oCAgICAgICAgICAgICAgICAgICAg8OL4tDSROlneI2N3naT8LRtJ5Ba2vFY3l2wafJnvFMcby5FpN0l1M5LKW9LH8WRncO/YzuGfNV+XV2nir12h6Bix7WzfNPt6NGlkc9xc4lzjtLiXOPeTmVEmZny9BSlaRtWNoUpu2LrAIK4ZXMcHMc5jhscxxa4dxGa2i0x4c8mOuSNrREx9W3YF0j11PYSOFWwbpTaTwkAv53UjHqr188qbVdB0+Xmnyz9PDqejOmVJXgCN2pJbtRSWbJzLdzhzCnY81cnh5XWdNz6Wfnjj3jw2EFdkBKAgICAgICAgICAgICAgICAgIMXpFjkNDTvmmOQya0fWe47GNHFaZLxSN5SNLpb6nJGOn/AI4HpJpDPXzGSZ2Q+zYPqMHAc+J3qpy5ZvPL6BodDj0lO2nn1n3Ylck4RgujIgICAgqjkc0hzSWuabtLSWuBGwgjYVmJmOYaXpW8dto4db0A6QuuLKatcBIcopcg2Q7mv4P57D37bHBqe75beXjuq9GnDvlw819Y9nSApjzqUBAQEBAQEBAQEBAQEBAQEHlxKvjp4nyyu1GMF3H8hxJ2WWtrRWN5dMWK+W8UpG8y4FplpLJiE5c67WMuImXuGjidxdxP5WVVmzTkl7/pvT66THt5mfMsAuCyEBAQEBARkRgRkKbsTz5dm6MtMDVM9mqHEzRN7DjtlYMs+Lhv45HirTT5u+Np8vD9Z6b+Hv8AEx/ln+JdAUpRCAgICAgICAgICAgICAghxQcT6SNKDVTGOJ30MDiG22PkFwZO4Zgee9Vepzd9u2PEPddE6bGnxfFvHzW/iGiqMu0oCwCyTwx1XiYbkzM8TsUmmn35lTarqsUntx/uxz8QlPvEd2S7xhpHoqL9Qz2/ySzEZR71++xScNJZp1DUVn8zJ0WJNfYPOoeJ+ofHd8u5cL6f1haabq8TO2SPu97hbao0xt5XNbRaN4QsNhB6MPrZIJY5YjqvicHMPMbjyIuDyK2paazu46jDXNjnHbxL6M0exZlZSxTs2SNuR8Lhk5p5ggjwVzS0Wru+b6nBbBltjt6Mkt3AQEBAQEBAQEBAQEBAQat0i437LQu1DaSc9VHbaLg67h3Nv4kLhqMnZRa9H0n4nUxE+I5lwaodmBwVRD6DaVolZal0NxBtGjnR5VYnF1glZSwuJAe5rpJH2yOqwEDVvcXLhsU3T4N47peZ6x1X4dvg0+7aGdBdDbtVNYTxb7O0eRYfmpvZDzM6m8sVi3QS4AmkrATbJlRHa5/mMOX4Umjauqn1hzTSTRStw+QMqoXRhxsx47cL/wCF4yvyOfJc7RMJeLJW/h4IILrSUqsMzS3DLHMDZy5DlyUfLj35Wuh1M452nwvXUR6CJEC6DqHQvi3aqKRxyI66Lws2QD/QfNT9HfzV5P8A1Hp+a5o/Sf8Ah1YKc8ulAQEBAQEBAQEBAQEEFBxXpmxoe1tjJ7NPELC+2STtH0DPVQNRE3vFY9Hq+jWppdLbNbzaePs5a3FnX7QBvwyKxOCrrXqeaLbzy9cVfG7fq/xZeq42wWjwn4up4rcW4eoLjMTHlY0yUvHyy9eEYe+pqIYGfWmkDRyB+s7wAJ8Ftjp3WiHHV6iMGG2SfT+30rQUbIIo4oxqsiYGsA3BosFcxG0bPm+S83tNreZehZaiDy4lh0NTE+KeNksbxZzXi4P6HntCxMbsxMxO8Pn7TfQ84ZU6rbuhlu6BxzNhtjcfiF/EEHjbheO2V1pc3xa8+Ya1LPZcpTqwrpn3aoWSPmeh0tu7HG/ourRJEGwaAV3UYnSOvYOl6t3MSDUA8yF209trwrOr4viaS/05/Z9DhW75+lAQEBAQEBAQEBAQEBBynT3oolraiWqp6oa8puY52kMBsBZr27BkNrT3rnOPneE3HrLVrFZ8Q5Tj2g+JUVzPTSaov9JEOuisN5cy+r96y0mkpNNTSzXLrV33VxzOZm0lvcsTES2rktTms7O6dD+iFTGRXVg6slhFPG5tpAHWvK8e6SLgDbYm63x4IrPdCLrepZM1PhTPDrC7qoQEBBrHSNg4q8NqGgXfEwyw7L68YLrDvF2/eWl43qkaXLOPLEvmkAuIABJJAAAJJJ2ABQnp9ojmfDquiHRXLIxr655p2nMRMsZrfvuNwzusTnuXSNN3TvZEydb+FXswxv8AWXQKPQTC4hYUzH85daUn8RXauCkeiqydU1WSd5vP24KzQTC5RY0zGc4i6I/6SszgpPozj6pq6TvF5+/LTMW6NpKaeGeje6Zkc0b3RvA61oa8EuaRk8C2y18t6jW0vbaJqtsXXIy4rY80czE8/Z1oKc8ylAQEBAQEBAQEBAQEBAQQQg1vH9BMMrbmemj1z/iRfQy34lzLa33rrExEt65LV8S17R7oioqSs9oL5KlrLGGOYMIY8G+u4gDXtlYEC23PK2sViJdL6i167S6Mt3AQEBAQW5wC119mqb+SxPhmPLjXQ7oywj26Vt7EtpQbEAjJ83f7o4WdyXDDT/KVz1PUeMVfu6z1ykKU61A65BImQeinmvl5IL6AgICAgICAgICAgICAgICAgICAgICDB6a4mKXD6qW9iInNj5yPGqweZC0yW2qkaXFOXNWv1Y3BYG09NBC3ZFE1veQMz3k3Pis0jaIaZ79+S1vq9vtC2cj2hBHtCCevQXYKmzgeaDOICAgICAgICAgICAgICAgICAgICAgtVM7I2Oe9zWNYLuc4hrQBvJOxYmdo5ZrWbTtXzLhvSLpl7dII4riCF12A5GR4y6xw3Dg3mSeAr82funaPD2HTOmfApN8n5pj9m/sqg4Ag5EXHcVYV8PI5YmLzH1T7QstE9egdegqE6CtktyBxIQbigICAgICAgICAgICAgICAgICAgIMXj+O09FEZJ3hoz1Rte8j3Wt3laXvFI3lI02myai/bjhw/TLTWfEXav2ULTdkYO22xzz7x9B6quy5pv+j2Og6Zj0sb+be//TVyuCzdH0SxTraVjSe1CNR3Gw+ofw28QVaae/dT9HiOr6acWomY8W5Zj2hd1Uv0jHykiNpcQM7ED1JsgqqY5IvtGOZwJHZ7rjJBaFTzQZTR9hlnbbMM7bjuy2Dzt6oN1QEBAQEBAQEBAQEBAQEBAQEBBBKDT9NNOoaEOjj1Zp7Ztv2I77DIRv4N2nLYM1Hy54pxHla9P6Xk1M91uK+/v+jiWMYtNVymWd7pHH4jkB8LRsaOQVfa82neXscGnx4K9mONo/t4Vq7IQe7CcSdTyB7cwcntvbWb+vBdMWSaW3Q9dpK6nH2T59G8UuIMlYHMdrA+YPAjcVaUvF43h4bUae+C81vC7HXvjN2m3FbODZsI0iD26slnA5ODrHwIO0IM7QPga20YawHcNiD0CdsOs8NABzfqgXPltKDLoCAgICAgICAgICAgICAgICAg0LpD009lvTUzvp3DtuyIiadne87hu2ndeLqM/bxHle9I6T+In4uX8sfy4tVTlzjmTmSSSSXOO1xJ2n+96r3r+I+WPELCMCbG4mxuIbtm0H0cq62a8BMUbSBNK4XjA2lgHvu5bt9lJwUtM7wperarBTH2XjefT6N6xLRnq5XMa4vAtq5C9iAc+asXjmKFKYZO0HC4t+hCD00OJOa/Vvfgg3fCZXOa1zhaxB8kGwtIIBGYOxBKAgICAgICAgICAgICAgICDA6Z4+KGkfLkXu7EIO97tngBdx5Bcs2Tsrum9P0k6rNGOPHr+j5+ralzi97nFz5CS5zs3Fzsy4qp8zvL6BFK4qRSvEQ8C2c9xDcQQhMxHl0HQbo5kqtWer14YDYtZm2aUceLGc9p3W2qXh0+/NnnuodYim+PD593ZaWligiDImtjjjb2WtADQBwCmxERHDzNrTad5YCSqDnOcdrjf9AstUObHILPAI5oLAwOO94nauYOqc25G/ggzlKzVFiLW/vLigs4fV9XI6J2zW7PK+Y8EGaQEBAQEBAQEBAQEBAQEBAQcW6XcXMta2AHs0zBcbusf2neTdQeJVdqr7229nsf9P6fswzlnzb+oc+ndmo0Lu87rSy5iAhu6p0ZaBBwjratoN7OponDK20TPB28Wjx4WnYMG3zS8t1Tqc3mcWKePWXWg1S1As1zSYpANpjcB32KDnbq/PagrZiI4oL7cT4FBW3FnfEgrGJB7xnmBY+aDacJrNdoado2IMkgICAgICAgICAgICAgIKXOsLncsSRG87PmbF681FRPMbnrpXvF+DnEtHgLDwVRed7TL6NpcfwsNae0QxjzmViG1p5QjCEGd0IwgVuIU8Lhdhdry7xqMGsQeRsG/eXXDTuvCv6ln+Dp7THmeIfSTW2CtHiEoCDneKYQxs0rbHJ5tmRkcx6EIMc7BSc26wHEk2QW/wBkvBzc5B7qLCmXBfrO5E5eiDOxiEO1ixl7AXsNg2AckHrbXNyDAAbi1uN0GxhAQEBAQEBAQEBAQEBAQYnSupMNBVyDaynkI79Q29Vpknasy76WnfmrX3mHzWFUvom6yUc58iMCDpXQdRh1VVTEfZQtYOH0jrn/AMY81M0sczLz/Xr/AC1p7uzKa8yICDVMUe32h7jY52F9lwA38ig87pLoLLrIJa8AILcLXTyNZGQ3WJGsQS0WBPjsQZ7CtHeqeHySGUtzaA3UYDxOZJQZ5AQEBAQEBAQEBAQEBAQax0ktkdhdS2GOSVzwxobE1z3kF7dYhrRc2FyueWJmkxCZoL0pqK2vPEcvmzFZ5ojquilp930rHxvPIBwyUWmn2/MvM/VpvxjnaP5Y9ldIPev32K3nFX2cK67NHiV5uJu3gHuuFpOCPRIr1O8fmhdbiTd4I8itPgSkV6pT/KHeOhTDpIqSeSWOSLrphqda1zHOY1gs4A52u52ak4KTWOVF1bU0z5Imk8RDo6kKsQWqqXUY93wtJQahVM1wUGKlM7cg0O53t+SC3rVB91o+9/sgvQ0xGcjtbkMm/wC6DJ4PLeoiA+L8kG6ICAgICAgICAgICAgICAgghBTLE1wIcA4HaHAEeRQYDENCMLnuZKGlJd9ZzYmxvPMuZY38VjZmLTDW6/oZwmS/Viopyf8ApSlw8pA5Y7YdIz3j1ZXRTo3w7Dy17I+vlGYlqLSPB4sFtVneBfmsxEQxfJa3luKy5iAgs1cWvG9vxNIHeRkg1OLZmgPsgsSOQY2uqrZIPfomC+pYfhDnHyI+ZCDfEBAQEBAQEBAQEBAQEBAQUSShoJcQ0DMkkAAcSU8ERMztDRdI+k+lgDhTj2pwy1gdWG/DW977otzUa+prHEcrrTdEzXiLZflj+f2cuxjTrEalxLqh8Q3MgJiaPEZnxJUW2a8+q8w9O02KOK7/AFnl56HTDEYSCyrnNt0jzK08iH3WIy3j1bZNBp7xzSPtw6xoB0hMrnCCoDYqi3Z1co5QBc6t9jht1fLfabizxfifLznUOm20/wA9Oa/030Fd1UlAQEGpVItJJ/Mf/UUHneUHlncg18EyPLt3u9yDcdC4e3IeDAPM/wDyg25AQEBAQEBAQEBAQEBAQQSg4j0kaWuqqh8EbiIIXltgcpHtyc53FoOQGzK/C1bqcs2ntjw9p0XQUw44zXj5p8fSGg1Elz3bFwiNlplt3StLLkhZF2nqHxvZJG4sexwcxw2hzTcFZidp3aZMcXrNZ8S+ndHcRFVSU84y66JriOBI7Q8DcK0pO9Yl4TPj+HktT2lkVs5CAg1KrP0kn8x/9RQeV5QYvFJbMdzyHjt9LoPDSMQbtoazsyniWjy1v1QbGgICAgICAgICAgICAgIPLikpZBM8bWRPcO8NJHyWJ8NqRvaHy11pO+/97VVRV9AnLtG0KCsbbETvyIIQLoPozozhczCKIOvnGXC/B73Pb6OCs8UbUh4nqFotqbzHu2ddEMQEGoVJ+kk/jd/UUHklKDCYo67mjhmfkPzQTTtyQbxok20Lzxk+TWoM4gICAgICAgICAgICAgIKJGggg5gix8UZjh8waSYY6hq5qd4t1b+wTftRnNjxxuLeII3KDam0vU4NV8XHEsbDKHnVuAfdvkD+7fjwWlqesJWHURE7SqK4p2+8IRhnNDtGZcRqWxMDhG0g1EgGTGbxf4zsA8dgK64sc2lA12trp6Tt+afD6Sp4WxsaxgDWsaGtA2AAWA9FYw8dMzM7yu3WWBBaqJgxpcdw/wCAg1Gc9oniSfNB45pQgwtUbvJQeqBBvOjDbU4PxOcfW35IMsgICAgICAgp1kDWQC5BGsggvQC9BBegdYg1bTnQ6nxSIB56qaMHqZmi5bf3XD3mcvIha2rFnbDmtjneHE8X6L8WgcQ2AVLdz6d7HAjd2XWcD4Ln8NNjWVnyvYR0b41MbOiZTt+KpkYPRus70Ws4Yny6V6lfHxWXQcF6JaZljVzyVJ3tjHURd1xd58CFmNPSGmTq+otG0cOg4bRwU0bYoI2QsbsawWHMnieZzXaI24hW3va872neXq65Zap61A61BicdqsmtHefkPzQaxVzILeEUjqidrM9UZyHg0bu87PFB4cR+2k/mOt+IoL9OEHQcKGrBEP8ALafMXPzQerWQTrIJ1kDWQLoJugXQWSUEFyCkuQQXoKesQUGVBSZkFDqhBZfWgbwgsuxNg2uHmgt/tZnxDzQT+1GfEEFTcRbxQVitB3oKxUoKxOgw+JSEucfLwQYKpeg2rAKQQRC+Tn9qTlwb4D1JQadV5yPPFxPmboPTTtyQbwypYxrGlwB1W2G07AL2CD0Ryh2w3QXA5BIKCboJugkFAugoIQRZBSWoKSxBSWIKDEEFDoQgoNMCgtPoGHcgtOwyM7kFBwqP4fmgj9mxj3R6oJFEwbggrFO0bkFWoAgs1UmoAd1+0eHfyQUkRubmRmgtMpKUbWhx/eJPpsQMUrLxP1TawueYG3++SDWhVMJ2hB7YqpgFgQgvxVTRsIQe2nxHtMAO1wAHG5QZ8SoKtdBUJEFQegkOQTrILlkEEIFkEWQQWoILUEFqCktQQWIKSxBBYgoMaCkxIKDEUFDoCg8k9LL7pt3i6DBVGD1oN43RW4ZsHlsCCz7DiI/wmnukZ+ZQXIcPrnG0kTWtIs7ttdccMiguN0Vj3xAdxcPkUFwaJQ/A4dz5P1QXG6IwcJPCWT9UHro9HYYnazA8HiXvJ9SgyrICEF0RlBUGFBUGlBWAgmyD0oCCEAoIQQUBBS5BCAUEIIQQgpKCCgpQAgFACCUEIKkBBUgqCCQgIJQEH//Z" alt="альтернативный текст" style="width: 100%;" >
                    </div>

                    <div class="col-md-8 col-xs-12">
                        <ul class="li-style mob-no-paddig-left" style="padding-left: 0px;">
                            <li>
                                <b>ОСТЕОПАТИЯ</b> –  это современный и безопасный метод лечения, один из самых популярных немедикаментозных методов лечения в Соединенных Штатах Америки и Европе вот уже более 130 лет. 
                            </li>
                            <li>
                                <b>ОСТЕОПАТИЯ</b> –  альтернативное направление медицины, которое рассматривает организм как единое целое, а причиной заболеваний считает нарушение структурно-анатомических соотношений отношений между различными органами и частями тела  и циркуляции жидкостей в теле. 
                            </li>
                            <li>
                                <b>ОСТЕОПАТИЯ </b> – это совокупность лечебно-диагностических методик, используемых для устранения причин и выявленных нарушений путем мануального воздействия на анатомические структуры черепа, позвоночника, крестца, мышечно-связочный аппарат, внутренние органы в целях восстановления их подвижности и оптимальной работы.
                            </li>
                            <li>
                                <b>ОСТЕОПАТИЯ</b>– это один из самых эффективных оздоровительных и восстановительных методов при проблемах опорно-двигательного аппарата, использующий преимущественно мягкие техники воздействия на мышцы, связки, суставы и кости. 
                            </li>
                        </ul>
                    </div>

                </div>




                <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                    <h2 style="font-weight: 600;text-decoration: underline;"> Почему остеопатия? </h2>

                </div> 



                <div class="col-md-12 col-xs-12" style="margin-top: 30px;">

                    <div class="col-xs-12 mob planshet block-with-image">
                        <img src="/images3/osteopat.jpg" alt="альтернативный текст" style="width: 100%;" >
                    </div>

                    <div class="col-md-8 col-xs-12">
                        <ul class="li-style mob-no-paddig-left" style="padding-left: 0px;">
                            <li >
                                <i class="fa fa-check"></i>
                                основана на идеальных знаниях об анатомии, физиологии, биомеханики и биодинамики; 
                            </li>
                            <li >
                                <i class="fa fa-check"></i>
                                точность ручной остеопатической диагностики (основанной на определении смещений костей, напряжений мышц, нарушений ритмов движения внутренних органов, натяжений фациальных оболочек и сухожилий) часто выше, чем данные, полученные при помощи инструментальных методов исследования; 
                            </li>
                            <li >
                                <i class="fa fa-check"></i>
                                всегда ищет первопричину проблемы. Устранение причины гораздо более эффективно, чем просто временное облегчение состояния.
                            </li>
                            <li >
                                <i class="fa fa-check"></i>
                                лечит не отдельные органы или проявления болезни, а весь организм в целом: костно-мышечную систему, внутренние органы и центральную нервную систему;
                            </li>
                            <li >
                                <i class="fa fa-check"></i>
                                использует мягкие нетравматические, но всегда целенаправленные техники. Остеопат точно выбирает на какой отдельный позвонок нужно произвести воздействие и в каком точном положении стоит это сделать.
                            </li>

                            <li >
                                <i class="fa fa-check"></i>
                                даёт быстрый лечебный эффект (улучшение после проведения от 1 до 3-х сеансов);
                            </li>
                            <li >
                                <i class="fa fa-check"></i>
                                обладает высокой эффективностью; 
                            </li>
                            <li >
                                <i class="fa fa-check"></i>
                                абсолютно безвредна и безболезненна;
                            </li>
                            <li >
                                <i class="fa fa-check"></i>
                                безлекарственный и нетравматический метод лечения.
                            </li>
                        </ul>
                    </div>



                    <div class="col-md-4 desctop">
                        <img src="/images3/osteopat.jpg" alt="альтернативный текст" style="width: 100%;" >
                    </div>

                </div>


            </div>
        </div>


</section>