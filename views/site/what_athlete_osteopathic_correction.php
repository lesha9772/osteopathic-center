<?php

use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Остеопатическая коррекция | Остеопатический центр';


$description = "Своевременная коррекция изменений позволит избежать в дальнейшем серьезных осложнений, затяжной реабилитации и хронизации патологических процессов в органах.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url' => Yii::$app->request->absoluteUrl,
    'description' => $description,
    'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);
?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class=" wtite background" style="text-align: justify; padding-bottom: 30px;">
            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Зачем спортсмену остеопатическая коррекция?</h2>
            </div>





            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <div class="col-md-4 col-xs-12 block-with-imagec">
                    <img src="<?= Url::to('/images3/спорт 3.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>
                <div class="col-md-8 col-xs-12">
                    <p>
                        Своевременная коррекция изменений позволит <b>избежать в дальнейшем серьезных осложнений,
                            затяжной реабилитации и хронизации патологических процессов в органах</b>.
                    </p>
                    <p>Как можно раннее после основного лечения обращение к
                        специалисту остеопатии обеспечит <b>наиболее эффективную и скорейшую реабилитацию</b> спортсмена.
                    </p>
                </div>
            </div>


            <div class="col-md-12 col-xs-12">
                <div class="col-md-12 col-xs-12">

                    <p>Остеопатия для спортсменов эффективно помогает <b>максимально восстановиться </b>после различных по степени тяжести травм и сильных физических
                        нагрузок.</p>
                    <p>Несколько раз в год спортсмену, даже при отсутствии жалоб, желательно проходить
                        комплексный осмотр у врача-остеопата.</p>
                    <p><b>Недостаточно только убедиться в пригодности спортсмена к экстремальным
                            нагрузкам, необходимо чтобы его организм находился в состоянии гармоничного
                            равновесия!</b></p>
                </div>
            </div>

            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Остеопатия для спортсменов решает проблемы:</h2>
            </div>    



            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <div class="col-md-4 col-xs-12 mob block-with-image">
                    <img src="<?= Url::to('/images3/спорт 4.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>

                <div class="col-md-8 col-xs-12">
                    <ul>
                        <li>Рубцы, послеоперационные спайки</li>
                        <li>Фасциальные и мышечные боли</li>
                        <li>Бурсит, капсулит, хондрит</li>
                        <li>Вывихи, подвывихи</li>
                        <li>Эпифизит, эпикондилит</li>
                        <li>Грыжа межпозвоночного диска</li>
                        <li> Повреждения связок (растяжения, надрывы)</li>
                        <li>Повреждения колена (крестообразных связок,менисков)</li>
                    </ul>
                </div>
                <div class="col-md-4 col-xs-12 desctop">
                    <img src="<?= Url::to('/images3/спорт 4.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>
            </div>



            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <div class="col-md-4">
                    <img src="<?= Url::to('/images3/спорт 5.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>
                <div class="col-md-8">
                    <ul>
                        <li>Люмбалгия,люмбоишиалгия, межреберная невралгия</li>
                        <li> Миозит, периартрит, периостит</li>
                        <li> Пубалгия</li>
                        <li>Тендиниты</li>
                        <li> Травмы любого генеза и характера</li>
                        <li>Реабилитация после оперативных вмешательств.</li>
                    </ul>
                </div>
            </div>



        </div>
    </div>
</section>
