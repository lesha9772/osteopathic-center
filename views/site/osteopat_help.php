<?php
$this->title = 'Чем поможет остеопат?';

use yii\helpers\Url;
?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">
            <div class="col-md-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Чем поможет остеопат ? </h2>

            </div> 



            <div class="col-md-12" style="margin-top: 30px;">
                <div class="col-md-4">
                    <img src="<?= Url::to('/images3/спорт 2.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>
                <div class="col-md-8">
                    <p>
                        1. Точность остеопатической диагностики позволяет <b>выявить первые признаки
                            нарушений</b> как в очаге поражения (сдвиги, тяги, деформации), так и на отдалении,
                        вызванных ударом или растяжением, даже если боль не выходит за пределы зоны травмы.
                    <p> 2. Остеопат <b>определяет ключевую зону</b>, которая пострадала больше всего, а также
                        все задействованные области, в которых процесс
                        только начинается.</p>
                    <p> 3. Устранив функциональные блоки и спазмы сосудов, остеопат <b>восстановливает </b>нормальную циркуляцию крови и лимфы
                        пораженных участков тела,<b> возвращает подвижность </b>травмированному суставу,улучшает двигательные функции организма спортсмена.
                    </p>
                </div>
            </div>
        </div>
</section>
