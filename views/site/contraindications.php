<?php

use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Противопоказания | Остеопатический центр';


$description = "Наша цель - вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать это состояние.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url'=>Yii::$app->request->absoluteUrl,
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);

?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-xs-12 wtite background" style="text-align: justify; padding-bottom: 30px;">

            <div class="col-md-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;"> Противопоказания к остеопатии</h2>

            </div>

            <div class="col-md-12" style="margin-top:30px;">
                <b>Мы не работаем, если у больного выявляются:</b>
            </div>

            <div class="col-md-12" style="margin-top:30px;">

                <div class="col-md-offset-1 col-md-4 text-center col-xs-12 mob planshet">
                    <img src="/images3/противопаказиня.jpg" style="width: 100%; border-radius: 6px;">
                </div>
                
                <div class="col-md-7 col-xs-12">
                    <ul class="ul-green-markers mob-no-paddig-left">
                        <li>
                            <span>Острые кровотечения – в этом случае надо в срочном порядке заниматься его остановкой.</span>
                        </li>
                        <li>
                            <span>
                                Острые кишечные и другие инфекции – дизентерия, сальмонеллез, дифтерия, холера и др. – требуют лечения в инфекционной больнице. ВИЧ инфекция в стабильной фазе не является противопоказанием.
                            </span>
                        </li>
                        <li>
                            <span>
                                Болезни сердца в острой стадии – инфаркт миокарда, гипертонический криз и т.п.
                            </span>
                        </li>
                        <li>
                            <span>
                                Инсульт.
                            </span>
                        </li>
                        <li>
                            <span>
                                Злокачественные опухоли – строгое противопоказание, кроме самых последних стадий, на которых возможно применение остеопатии с целью облегчения острой боли.
                            </span>
                        </li>
                        <li>
                            <span>
                                Лейкоз и другие заболевания крови – противопоказание вплоть до наступления полной ремиссии.
                            </span>
                        </li>
                        <li>
                            <span>
                                Острые психические заболевания – шизофрения, острый психоз и т.п.
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-offset-1 col-md-4 text-center desctop">
                    <img src="/images3/противопаказиня.jpg" style="width: 100%; border-radius: 6px;">
                </div>
            </div>

            <div class="col-md-12 xs-section" style="margin-top:30px;">
                <div class="col-md-4 col-xs-12 block-with-image" >
                    <img src="/images3/противопаказиня 2.jpg" style="width: 100%; border-radius: 6px;">
                </div>
                <div class="col-md-8 col-xs-12" style="font-style: italic; font-size: 18px;">

                    Кроме того, некоторые запущенные

                    заболевания требуют лечения в

                    больнице. Мы не рискуем своим

                    дипломом и репутацией. Поэтому если

                    на приеме у пациента обнаруживаются

                    противопоказания к лечению, мы

                    сообщаем ему об этом, а продолжить

                    лечение остеопатией можно после

                    выписки.
                </div>
            </div>
        </div>

    </div>

</section>