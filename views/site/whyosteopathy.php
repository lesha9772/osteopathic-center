<?php

use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Почему остеопатия? | Остеопатический центр';

$description = "Наша цель - вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать это состояние.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url'=>Yii::$app->request->absoluteUrl,
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);

?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-xs-12 wtite background" style="text-align: justify; padding-bottom: 30px;">
            <div class="row">

                <!--<h1> ОСТЕОПАТИЯ</h1>-->
                <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                    <h2 style="font-weight: 600;text-decoration: underline;"> Почему остеопатия?</h2>

                </div>

                <div class="col-md-12 col-xs-12" style="margin-top:30px;">
                    <div class="col-md-4 mob planshet">
                        <img src="<?= Url::to('/images3/главная 2.jpg') ?> " alt="" style="width: 100% ; border-radius:6px;" >
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <ul class="li-style mob-no-paddig-left">
                            <li > &#8211 <b>основана на идеальных знаниях </b>об анатомии, физиологии, биомеханики и биодинамики; 
                            </li>
                            <li > &#8211 <b>точность ручной остеопатической диагностики</b> (основанной на определении смещений костей, напряжений мышц, нарушений ритмов движения внутренних органов, натяжений фациальных оболочек и сухожилий) часто выше, чем данные, полученные при помощи инструментальных методов исследования; 
                            </li>
                            <li > &#8211 всегда <b>ищет первопричину проблемы</b>. Устранение причины гораздо более эффективно, чем просто временное облегчение состояния.
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 desctop">
                        <img src="<?= Url::to('/images3/главная 2.jpg') ?> " alt="" style="width: 100% ; border-radius:6px;" >
                    </div>
                </div>

                <div class="col-md-12 " style="margin-top:30px;">
                    <div class="col-md-5 col-xs-12 block-with-image">
                        <img src="<?= Url::to('/images3/почему остео 2.jpg') ?> " alt="альтернативный текст" style="width: 100%; border-radius: 6px;" >
                    </div>

                    <div class="col-md-7 col-xs-12">
                        <ul class="li-style mob-no-paddig-left">

                            <li > &#8211 лечит не отдельные органы или проявления болезни, а <b>весь организм в целом:</b> костно-мышечную систему, внутренние органы и центральную нервную систему;
                            </li>
                            <li > &#8211 использует <b>мягкие нетравматические, но всегда целенаправленные техники</b>. Остеопат точно выбирает на какой отдельный позвонок нужно произвести воздействие и в каком точном положении стоит это сделать.
                            </li>
                            <li > &#8211 даѐт <b>быстрый лечебный эффект </b>(улучшение после проведения от 1 до 3-х сеансов);
                            </li>

                            <li > &#8211 обладает <b> высокой эффективностью</b>;
                            </li>

                            <li > &#8211 абсолютно <b>безвредна и безболезненна</b>;
                            </li>
                            <li > &#8211 <b>безлекарственный и нетравматический</b> метод лечения.
                            </li>
                        </ul>

                    </div>

                </div>






            </div>
        </div>

    </div>

</section>