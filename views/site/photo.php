<?php
$this->title = 'Фотогалерея';
?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div id="content-block" class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">
            <div class="col-md-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Фотогалерея </h2>

            </div> 
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-content">
                        <div class="lightBoxGallery" id="links">

                            <?php if ($model) : ?>
                              <?php foreach ($model as $image): ?>

                                <a href="<?= \app\models\Photo::ImageSmallCrop($image->img, 1200, 1200) ?>" title="<?= $image->title ?>" data-gallery=""><img src="<?= \app\models\Photo::ImageSmallCrop($image->img, 150, 150) ?>"></a>

                              <?php endforeach; ?>
                            <?php endif; ?>



                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>
</section>
