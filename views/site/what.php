<?php

use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Что и как мы лечим? | Остеопатический центр';

$description = "Наша цель - вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать это состояние.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url' => Yii::$app->request->absoluteUrl,
    'description' => $description,
    'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);
?>


<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-md-12 col-xs-12 wtite background" style="text-align: justify; padding-bottom: 30px;">

            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Что и как мы лечим? </h2>
            </div> 

            <!--<hr>-->
            <div class="col-md-12 col-xs-12" style=" /*   width: 70%;padding-left: 20px; float: left;*/">


                <p style="text-align: left; font-size: 20px; font-style: italic;  text-decoration: underline;"class="title-row mob planshet desctop" ><b>
                        Что мы лечим?
                    </b></p>

                <div class="col-md-6 col-xs-12 mob planshet desctop block-with-image" style=" /*padding-left: 40px;*/  /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/что мі лечим.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px;">
                </div>

                <div class="col-md-offset-1 col-md-5 col-xs-12" style=" /*   width: 70%;padding-left: 20px; float: left;*/">
                    <p style="text-align: left; font-size: 20px; font-style: italic;  text-decoration: underline;"class="title-row  planshet desctop" ><b>
                            Что мы лечим?
                        </b></p>

                    <ul style="font-size: 16px;" class="ul-green-markers">
                        <li><span><b>остеохондроз</b>, спондилез;</span></li>
                        <li><span>протрузия межпозвонкового диска;</span></li>
                        <li><span><b>грыжа межпозвоночная;</b></span></li>
                        <li><span>ишиас (невралгия седалищного нерва);</span></li>
                        <li><span>люмбаго (боли и прострелы в пояснице);</span></li>
                        <li><span>цервикобрахиалгии (боли в шее и руке);</span></li>
                        <li><span>торакалгия (боли в грудном отделе позвоночника);</li>
                    </ul>
                </div>
                <div class="col-md-6 col-xs-12 desctop" style=" padding-left: 40px;/*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/что мі лечим.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px;">
                </div>
            </div>

            <div class="col-md-12 col-xs-12 xs-section" style=" /*   float: left;width: 30%;padding-left: 0px; */"> 
                <div class="col-md-offset-1 col-md-5 col-xs-12  block-with-image" style="/* padding-left: 40px;*/  /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/что мі лечим 2.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px;">
                </div>

                <div  class="col-md-6 col-xs-12"  style="/*    width: 70%;padding-left: 20px; float: left; */">
                    <ul style="font-size: 16px;" class="ul-green-markers">        
                        <li><span>различные деформации (искривления) позвоночника и <b>нарушения осанки</b>: сколиоз, кифоз, лордоз, гиперлордоз, гиперкифоз, "плоская спина"; </span></li>
                        <li><span><b>спондилолистез </b>(смещение позвонков / нестабильность позвоночника);</span></li>
                        <li><span>головокружения;</span></li>
                        <li><span><b>головные боли.</b></span></li>
                    </ul>
                </div>
            </div>


            <div class="col-md-12 col-xs-12 xs-section" style="/*margin-top:30px;*/">
                <div style="margin-top:25px; margin-bottom: 10px;">
                    Специалисты нашего центра помогут вам избавиться <b>от дискомфортных и болевых симптомов и проявлений заболеваний позвоночника и суставов.</b>
                </div>
            </div>


            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Как мы лечим? </h2>
            </div> 


            <p style="text-align: left; font-size: 20px;"><b>
                    Разработка программы лечения
                </b></p>
            <br />


            <div class="col-md-12 col-xs-12 " style=" /*   float: left;width: 30%;padding-left: 0px; */"> 
                <div class="col-md-offset-1 col-md-5 col-xs-12  block-with-image" style=" /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/как лечим 1.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px;">
                </div>

                <div  class="col-md-6 col-xs-12"  style="/* padding-left: 40px; text-align: justify;  */ /*    width: 70%;padding-left: 20px; float: left; */">
                    1. <b>Консультация, осмотр,</b> при необходимости – направление на дополнительные методы исследования, постановка диагноза, разработка программы лечения, определение прогноза по лечению.  ЖЕЛАТЕЛЬНО приходить на первичную консультацию со всеми имеющимися у Вас данными предыдущих исследований – рентгеновскими снимками, данными УЗИ, компьютерной и магнито-резонансной томографии, общими, биохимическими анализами крови,  мочи и др.
                </div>
            </div>   

            <div class="col-md-12 col-xs-12 xs-section" style="/* margin-top: 30px; */ /*   float: left;width: 30%;padding-left: 0px; */"> 

                <div class=" col-md-6 col-xs-12 mob planshet block-with-image" style="/*padding-left: 40px;*/ /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/как лечим 2.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px;">
                </div>


                <div  class="col-md-6 col-xs-12 "  style=" text-align: justify; /*    width: 70%;padding-left: 20px; float: left; */">
                    2. <b>Лечение по разработанной программе,</b> которая:
                    <ul class="li-style mob-no-paddig-left">
                        <li> &#8211 подбирается <b>строго  индивидуально </b> специалистом центра в зависимости от возраста, тяжести заболевания, выраженности болевого синдрома, давности заболевания, сопутствующих заболеваний и др.факторов ;</li>
                        <li> &#8211 включает <b>основной</b> и <b>дополнительные </b>методы лечения; </li>
                    </ul> 
                </div>
                <div class=" col-md-6 col-xs-6 desctop" style="padding-left: 40px; /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/как лечим 2.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px;">
                </div>
            </div> 




            <div class="col-md-12 col-xs-12 " style=" /*margin-top:30px; */ /*   float: left;width: 30%;padding-left: 0px; */"> 

                <div class=" col-md-6 col-xs-12 block-with-image text-center" style=" /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/как лечим 3.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px;">
                </div>
                <div  class="col-md-6 col-xs-12"  style="text-align: justify; /*    width: 70%;padding-left: 20px; float: left; */">
                    <ul class="li-style mob-no-paddig-left">
                        <li> &#8211 может включать только <b>основной метод лечения – остеопатический сеанс; </b></li>
                        <li> &#8211 при необходимости программа дополняется сразу, постепенно или по необходимости <b>дополнительными методами лечения: кинезитерапией,  физиотерапией, массажем, вытяжением позвоночника, йога-терапией, медикаметозным лечением</b> (в большинстве случаев – натуропатическими лекарственными препаратами, при необходимости – традиционными лекарственными средствами );</li>
                        <li> &#8211 даёт <b>быстрый лечебный эффект;</b></li>
                        <li> &#8211 обладает <b>высокой эффективностью;</b></li>
                        <li> &#8211 даёт <b>стойкий результат.</b></li>
                    </ul>
                </div>
            </div> 
            <div class="col-md-12 col-xs-12 xs-section" style="/* margin-top:30px;*/ /*   float: left;width: 30%;padding-left: 0px; */"> 
                <div class="col-md-6 col-xs-12 mob block-with-image" style="/*padding-left: 40px;*/ /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/1.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px; ">
                </div>

                <div  class="col-md-6 col-xs-12"  style="/*    width: 70%;padding-left: 20px; float: left; */">
                    3. Рекомендации по образу жизни, режиму двигательной активности, питанию, спортивным нагрузкам, медикаментозной поддержке, которые разрабатываются индивидуально для каждого пациента.
                </div>
                <div class="col-md-6 col-xs-6  desctop" style="padding-left: 40px; /*   float: left;width: 30%;padding-left: 0px; */"> 
                    <img src="<?= Url::to('/images3/1.jpg') ?>" style="width: 100%; max-width: 300px;border-radius: 6px; ">
                </div>

            </div> 


        </div>
    </div>

</section>
