<?php
$this->title = 'Медикаментозное лечение';
?>
<section class="container features section-top">
    <div class="row">
        <div class="col-lg-12 wtite background" style="padding-bottom: 15px;">

            <!--<h1> Дополнительные методы</h1>-->
            <h1> Медикаментозное лечение</h1>
            <div style="    float: left;width: 30%;padding-left: 0px;"> 
                <img src="https://doctorkutuzov.ru/upload/54c8dc2516bcb_.jpg" alt="альтернативный текст" style="width: 100%;" >
            </div>
            <div style="    width: 70%;padding-left: 20px; float: left;">
            <p>
                <b>Медикаментозное лечение:</b>
            </p>
            <ul class="li-style" style="padding-left: 0px;">
                <li>- применяется в качестве вспомагательного метода в программах комплексного лечения;</li>
                <li>- предпочтение отдаем натуропатическим медицинским препаратам, которые не вызывают побочных эффектов, не дают аллергии, не оказывают токсического действия на организм;
                </li>
                <li>- при необходимости используем традиционные медикаментозные средства (нестероидные противовоспалительные средства, хондропротекторы и др.)</li>
            </ul>
            </div>
        </div>

        <br />
        <br />
    </div>
</section>
