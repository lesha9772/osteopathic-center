<?php
$this->title = 'Физиотерапия';
?>
<section class="container features section-top">
    <div class="row">
        <div class="col-lg-12 wtite background">

            <!--<h1> Дополнительные методы</h1>-->
            <h1> Физиотерапия</h1>
            <div>
                <p>
                В основе клинических эффектов физиотерапии лежит нормализующее влияние на обмен веществ. 
            </p>
            </div>
            <div style="    float: left;width: 30%;padding-left: 0px;"> 
                <img src="https://doctorkutuzov.ru/upload/54c8dc2516bcb_.jpg" alt="альтернативный текст" style="width: 100%;" >
            </div>
            <div style="    width: 70%;padding-left: 20px; float: left;">
            
            <p>
                <b>В результате:</b>
            </p>
            <ul style="list-style-type: none;">
                <li>
                    <i class="fa fa-check"></i> исчезают  или уменьшаются болевые синдромы;
                </li>
                <li>
                    <i class="fa fa-check"></i> движение на специально разработанных лечебных  тренажерах;
                </li>
                <li>
                    <i class="fa fa-check"></i>  уменьшается активность воспалительных процессов; 
                </li>
                <li>
                   <i class="fa fa-check"></i> улучшается трофика органов и тканей;
                </li>
                <li>
                    <i class="fa fa-check"></i> усиливаются репаративные процессы. 
                </li>
            </ul>

            <p>
                <b>Преимущества:</b>
            </p>
            <ul style="list-style-type: none;">
                <li >
                    <i class="fa fa-check"></i> нет побочных эффектов;
                </li>
                <li>
                    <i class="fa fa-check"></i> не вызывает аллергии;
                </li>
                <li>
                    <i class="fa fa-check"></i> не оказывает токсического влияния на органы и ткани.
                </li>
            </ul>
                    </div>
        </div>
        <div class="col-lg-12 wtite background">
            <p>В нашем центре используется как дополнительный метод по индивидуальной программе.</p>
        </div>

        <br />
        <br />
    </div>
</section>
