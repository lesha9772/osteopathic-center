<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\bootstrap\Collapse;
use yii\helpers\Html;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Остеопатический центр | Профессиональное лечение позвоночника';

$description = "Наша цель - вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать это состояние.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url'=>Yii::$app->request->absoluteUrl,
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);


$this->registerJsFile('js/owl.carousel.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('js/slider_init.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile('css/owl.carousel.css');
//$this->registerCssFile('css/unslider-dots.css');
?>



<section id="slideshow" class="">
    <div class="container">
        <div class="row">
            <div class="slider-block">
                <div id="slider-main" class="owl-carousel owl-theme" style="">
                    <div class="slide-image">
                        <div class="slide" style="background-image: url('<?= Url::to('/img/sliders/osteopatiya.jpg') ?>')">
                            <div class="g-newsslider-overlay">
                                <div class="g-newsslider-preview-title">Остеопатия</div>
                                <!--<div class="g-newsslider-preview-desc">Text to do</div>-->
                                <span class="g-newsslider-button"> <a target="_self" href="<?= Url::to('/osteopathywhat') ?>" title="Подробнее" class="button ">Подробнее</a> </span>
                            </div>
                        </div> 
                    </div>
                    <div class="slide-image">
                        <div class="slide" style="background-image: url('<?= Url::to('/img/sliders/IMG_20160603_122423.jpg') ?>')">
                            <div class="g-newsslider-overlay">
                                <div class="g-newsslider-preview-title">Кинезитерапия</div>
                                <!--<div class="g-newsslider-preview-desc">Text to do</div>-->
                                <span class="g-newsslider-button"> <a target="_self" href="<?= Url::to('/kinesitherapy') ?>" title="Подробнее" class="button ">Подробнее</a> </span>
                            </div>
                        </div> 
                    </div>
                    <div class="slide-image">
                        <div class="slide" style="background-image: url('<?= Url::to('/img/sliders/massage.jpg') ?>')">
                            <div class="g-newsslider-overlay">
                                <div class="g-newsslider-preview-title">Массажные техники</div>
                                <!--<div class="g-newsslider-preview-desc">Text to do</div>-->
                                <span class="g-newsslider-button"> <a target="_self" href="<?= Url::to('/massage-techniques') ?>" title="Подробнее" class="button ">Подробнее</a> </span>
                            </div>
                        </div> 
                    </div>
                    <div class="slide-image">
                        <div class="slide" style="background-image: url('<?= Url::to('/img/sliders/yogoterapiya.jpg') ?>')">
                            <div class="g-newsslider-overlay">
                                <div class="g-newsslider-preview-title">Йога-терапия</div>
                                <!--<div class="g-newsslider-preview-desc">Text to do</div>-->
                                <span class="g-newsslider-button"> <a target="_self" href="<?= Url::to('/yoga-therapy') ?>" title="Подробнее" class="button ">Подробнее</a> </span>
                            </div>
                        </div> 
                    </div>
                </div>
                <ul class="g-newsslider-pagination">
                    <li class="selected">1</li>
                    <li class="">2</li>
                    <li class="">3</li>
                    <li class="">4</li>
                    <!--<li class="">5</li>-->
                    <!--<li class="">6</li>-->
                </ul>

                <div id="slider-items" class="slider-items" style="">
                    <div  class="g-newsslider-scrollbar">
                        <ul class="g-newsslider-headlines news-headlines">
                            <li class="nh-anim first selected">
                                <div class="arrow-container">
                                    <i class="fa fa-chevron-right"></i>
                                </div>
                                <span class="g-newsslider-headlines-title">Остеопатия</span>
                            </li>
                            <li class="nh-anim">
                                <div class="arrow-container">
                                    <i class="fa fa-chevron-right"></i>
                                </div>
                                <span class="g-newsslider-headlines-title">Кинезитерапия</span>
                            </li>
                            <li class="nh-anim">
                                <div class="arrow-container">
                                    <i class="fa fa-chevron-right"></i>
                                </div>
                                <span class="g-newsslider-headlines-title">Массажные техники</span>
                            </li>
                            <li class="nh-anim">
                                <div class="arrow-container">
                                    <i class="fa fa-chevron-right"></i>
                                </div>
                                <span class="g-newsslider-headlines-title">Йога-терапия</span>
                            </li>
                        </ul>
                        <div class="g-newsslider-navigation clearfix">
                            <div class="slider-btn prev"><i class="fa fa-arrow-up"></i></div>
                            <div class="slider-btn next"><i class="fa fa-arrow-down"></i></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>

<section >
    <div class="container" style="    background: #fff;">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <!--<div class="">-->
                    <div id="main-section2 " class="left-block">
                        <div class="g-content">
                            <div class="moduletable ">
                                <!--<h2 class="g-title" style="padding: 15px 15px 15px 20px;">Міні Галерея</h2>-->
                                <div class="g-imagegrid " >
                                    <div class="col-md-12 col-xs-12">
                                        <p><b>
                                                <h3 style="
                                                    text-align: justify;
                                                    font-style: italic;
                                                    color: #000;
                                                    ">Наша цель -  вернуть позвоночник в его оптимальное состояние здоровья и научить пациента сохранять и поддерживать  это состояние.</h3>
                                            </b></p>

                                    </div>
                                    <div class="mob col-md-12 col-xs-12">
                                        <div class=" g-pricingtable-col-item g-pricingtable-accent1" id="g-pricingtable-col-item-1mob">
                                            <ul class="g-pricingtable ">
                                                <!--                                                <li class="g-pricingtable-plan">
                                                                                                    <a target="_self" href="#" title="Записаться на прием">                Консультация Врача
                                                                                                    </a>            </li>-->
                                                                                                <!--<li class="animated pulse g-pricingtable-price"><b>100</b> <span style="font-weight: 100;">грн.</span> </li>-->         
                                                                                                <!--<li class="g-pricingtable-item g-pricingtable-item-0 "><i class="fa fa-check"></i> ИНДИВИДУАЛЬНЫЙ ПОДХОД</li>
                                                                                                <li class="g-pricingtable-item g-pricingtable-item-1 "><i class="fa fa-check"></i> СОВРЕМЕННЫЕ МЕТОДИКИ</li>
                                                                                                <li class="g-pricingtable-item g-pricingtable-item-2 "><i class="fa fa-check"></i> ВЫСОКИЙ ПРОФЕССИОНАЛИЗМ</li> -->
                                                                                                <!--<li class="animated fadeInDown"> <img src="/img/doc.jpg" style="width:100%;"></li>-->

                                                <li> <b>Прием проводит врач-невролог, остеопат.</b><br>
                                                    <b>Цюрко Артем Сергеевич</b><br><br>
                                                    <p>Окончил Винницкий Медицинский Университет им. М.И. Пирогова.</p>
                                                    <p>Прошел 4-ех годичный курс обучения в испанской школе остеопатии.</p>
                                                    <p>Более 10 лет практики в области неврологии, из которых большую часть работал в стационаре.</p>
                                                    <p>Имеет опыт лечения сложных заболеваний позвоночника. Использует индивидуальный подход и современные методики.</p> 
                                                </li>


                                                <li class="g-pricingtable-cta">
                                                    <a target="_self" href="#" title="ЗАПИСЬ НА ПРИЕМ ПО ТЕЛЕФОНУ" style="    text-align: center;" class="button ">
                                                        <!--<i class="fa fa-arrow-right"></i>-->
                                                        ЗАПИСЬ НА ПРИЕМ ПО ТЕЛЕФОНУ<br>(0432) 655-044 <br>(068) 288-44-88<br>(063) 525-58-55
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>

                                        <div class="slider-clinica-images">
<?php if ($slids_small): ?>
  <?php foreach ($slids_small as $one_small_slide): ?>
                                                <div class="slide-image" >
                                                    <img src="<?= \app\models\SliderImages::ImageSmallCrop($one_small_slide->img, 450, 450) ?>" style="width: 100%">
                                                </div> 

  <?php endforeach; ?>
<?php endif; ?>
                                        </div>
                                        <div class="clinica-desc">
                                            <p>15 05 2016</p>
                                            <p class="g-simplecontent-item-content-title">О Центре</p>
                                            <p class="index-about-clinic">Основное направление деятельности центра – лечение заболеваний

                                                позвоночника. Главная методика лечения: проведение остеопатических

                                                сеансов, кинезитерапия и массаж. Приходя к нам, пациенты попадают не

                                                просто в медицинский центр, а в атмосферу домашнего уюта.

                                                Доброжелательный персонал, профессиональные реабилитологи и

                                                массажисты подарят заряд положительных эмоций и хорошего настроения.

                                                Мы занимаемся профессиональным лечением позвоночника с удовольствием

                                                для нас и наших пациентов.</p>

                                            <div>
                                                <a target="_self" href="/" title="Подробнее" class="button ">Подробнее</a>
                                            </div>

                                        </div>

                                        <div class="moduletable nopaddingall nomarginall">
                                            <div class="g-gridcontent ">
                                                <div class="g-grid">
                                                    <div class="g-block g-gridcontent-title-desc">
                                                    </div>
                                                    <div class="g-block g-gridcontent-readmore">
                                                    </div>
                                                </div>

                                                <div class="g-gridcontent-wrapper g-gridcontent-2cols">
                                                    <div class="g-gridcontent-item">
                                                        <a href="<?= Url::to('/contacts') ?>">
                                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                                <i class="fa fa-map-marker"></i>
                                                                <span class="g-gridcontent-item-title">Как добраться?</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="g-gridcontent-item ">
                                                        <a href="<?= Url::to('/site/read-important') ?>">
                                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1 ">
                                                                <i class="fa fa-exclamation-triangle "></i>
                                                                <span class="g-gridcontent-item-title">Важно знать!</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="g-gridcontent-item">
                                                        <a href="/comments">
                                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                                <i class="fa fa-comments"></i>
                                                                <span class="g-gridcontent-item-title">Отзывы пациентов</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="g-gridcontent-item">
                                                        <a href="<?php echo Url::to("photo") ?>">
                                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                                <i class="fa fa-camera"></i>
                                                                <span class="g-gridcontent-item-title">Фото Галерея</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>		
                                        </div>




                                    </div>
                                    <br />
                                    <br />

                                    <div class="col-md-12 col-xs-12" style="margin-top:70px;">
                                        <p style="text-align: left; font-size: 20px;"><b>
                                                Профессиональная  помощь при:
                                            </b></p>

                                        <div class="col-md-6 col-xs-6">
                                            <ul class="animated bounceInRight ul-green-markers"style="list-style: none; padding-left: 0px; font-size: 16px;    line-height: 32px;">
                                                <li>
<!--                                                        <i class="fa fa-check"></i> - боли в спине;-->
                                                    <span>боли в спине</span>
                                                </li>
                                                <li>
                                                    <span>боли в  шее</span>
                                                </li>
                                                <li>
                                                    <span>боли в пояснице</span>
                                                </li>
                                                <li>
                                                    <span>боли в грудном отделе</span>
                                                </li>
                                                <li>
                                                    <span>онемении пальцев конечностей</span>
                                                </li>
                                                <li>
                                                    <span>прострелах в любом отделе позвоночника</span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-xs-6 image-block1">
                                            <img src="<?= Url::to('/images3/главная 1.jpg') ?>" style="border-radius: 6px; ">
                                        </div>

                                    </div>

                                    <div class="col-md-12 col-xs-12" style="padding-top: 30px; margin-top:20px;">
                                        <p style="text-align: left; font-size: 20px;" class="title-row mob"><b>
                                                Нетравматическая методика  безоперационного  лечения: 
                                            </b></p>
                                        <div class="col-md-6 col-xs-6 image-block1">
                                            <img src="<?= Url::to('images3/главная 2.jpg') ?>" style=" border-radius: 6px;">
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <p style="text-align: left; font-size: 20px;" class="title-row"><b>
                                                    Нетравматическая методика  безоперационного  лечения: 
                                                </b></p>
                                            <ul class="animated bounceInLeft ul-green-markers"style="list-style: none; padding-left: 0px; font-size: 16px;    line-height: 32px;">
                                                <li>
                                                    <span> всех видов остеохондроза</span>
                                                </li>
                                                <li>
                                                    <span> всех видов радикулита</span>
                                                </li>
                                                <li>
                                                    <span> протрузий межпозвоночных дисков</span>
                                                </li>
                                                <li>
                                                    <span> межпозвоночных грыж</span>
                                                </li>
                                                <li>
                                                    <span> различных искривлений позвоночника и нарушений осанки</span>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-xs-12" style="margin-top:50px;">

                                        <p style="text-align: left; font-size: 20px;" class="title-row mob"><b>
                                                Реабилитация после травм и операций на  позвоночнике.
                                            </b></p>
                                        <div class="col-md-6 col-xs-6">
                                            <p style="text-align: left; font-size: 20px;" class="title-row"><b>
                                                    Реабилитация после травм и операций на  позвоночнике.
                                                </b></p>

                                            <ul class="animated bounceInRight ul-green-markers"style="list-style: none; padding-left: 0px; font-size: 16px;    line-height: 32px;">
                                                <li>
<!--                                                        <i class="fa fa-check"></i> - боли в спине;-->
                                                    <span>проведение сеансов остеопатии</span>
                                                </li>
                                                <li>
                                                    <span> индивидуальные занятия с реабилитологом</span>
                                                </li>
                                                <li>
                                                    <span>лечебный массаж</span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-xs-6 image-block1">
                                            <img src="<?= Url::to('/images3/главная 3.jpg') ?>" style=" border-radius: 6px;">
                                        </div>

                                    </div>


                                    <div class="col-md-12 col-xs-12" style="margin-top:30px; padding: 30px 15px 15px 15px;">
                                        <h2>Самые частые вопросы</h2>
<?php
$items = [
    [
        'encodeLabels' => false,
        'label' => 'Лечите ли вы межпозвоночные грыжи?' . Html::decode('<div class="toogle-icon active"><i class="fa fa-minus"></i></div><div class="toogle-icon disable"><i class="fa fa-plus"></i></div>'),
        'content' => 'Да, мы лечим межпозвоночные грыжи. Используем различные методики  безоперационного лечения.',
        'contentOptions' => [],
        'options' => []
    ],
    [
        'label' => 'Занимаетесь ли вы лечением детей?' . Html::decode('<div class="toogle-icon active"><i class="fa fa-minus"></i></div><div class="toogle-icon disable"><i class="fa fa-plus"></i></div>'),
        'content' => 'Вопрос о лечении детей решается после консультации врача в индивидуальном порядке.',
        'contentOptions' => [],
        'options' => []
    ],
    [
        'label' => 'Чем отличается консультация врача от сеанса остеопатии?' . Html::decode('<div class="toogle-icon active"><i class="fa fa-minus"></i></div><div class="toogle-icon disable"><i class="fa fa-plus"></i></div>'),
        'content' => 'Консультация врача – это классический прием врача, который включает опрос и осмотр пациента, постановку диагноза, при необходимости – направление на дополнительные методы исследования (МРТ, рентген), выбор схемы лечения, продолжительность – до 30 мин. Сеанс остеопатии – лечебная или профилактическая процедура, которую проводит врач после постановки диагноза, продолжительность – до 60 мин.',
        'contentOptions' => [],
        'options' => []
    ],
    [
        'label' => 'Занимаетесь ли вы реабилитацией после операций на позвоночнике? ' . Html::decode('<div class="toogle-icon active"><i class="fa fa-minus"></i></div><div class="toogle-icon disable"><i class="fa fa-plus"></i></div>'),
        'content' => 'Да, мы занимаемся реабилитацией после операций на позвоночнике.',
        'contentOptions' => [],
        'options' => []
    ],
    [
        'label' => 'Можно ли у вас лечить сколиоз и как? ' . Html::decode('<div class="toogle-icon active"><i class="fa fa-minus"></i></div><div class="toogle-icon disable"><i class="fa fa-plus"></i></div>'),
        'content' => 'Да, мы занимаемся лечением сколиозов, используя остеопатию и кинезитерапию.',
        'contentOptions' => [],
        'options' => []
    ],
];

echo Collapse::widget([
    'encodeLabels' => false,
    'items' => $items
]);
?>
                                    </div>


                                    <div class="col-md-12 col-xs-12" style="margin-top:30px; padding: 30px 15px 30px 15px;">
                                        <img style="width: 100%; border-radius: 6px;     max-width: 500px;"  src="/images3/thinkstockphotos-451559449-105931.jpg">
                                    </div>




                                </div>


                            </div>		
                        </div>

                    </div>
                    <!--</div>-->


                    <div  class="right-block desctop" style="/*width: 30%; float: right;     padding-bottom: 30px; */">
                        <div class="g-pricingtable-col-item g-pricingtable-accent1" id="g-pricingtable-col-item-1">
                            <ul class="g-pricingtable ">
                                <!--                                <li class="g-pricingtable-plan">
                                                                    <a target="_self" href="#" title="Записаться на прием">                Консультация Врача
                                                                    </a>            </li>
                                                                <li class="animated pulse g-pricingtable-price"><b>100</b> <span style="font-weight: 100;">грн.</span> </li>         -->
                                                                <!--<li class="g-pricingtable-item g-pricingtable-item-0 "><i class="fa fa-check"></i> ИНДИВИДУАЛЬНЫЙ ПОДХОД</li>
                                                                <li class="g-pricingtable-item g-pricingtable-item-1 "><i class="fa fa-check"></i> СОВРЕМЕННЫЕ МЕТОДИКИ</li>
                                                                <li class="g-pricingtable-item g-pricingtable-item-2 "><i class="fa fa-check"></i> ВЫСОКИЙ ПРОФЕССИОНАЛИЗМ</li> -->
                                                                <!--<li class="animated fadeInDown"> <img src="/img/doc.jpg" style="width:100%;"></li>-->

                                <li> <b>Прием проводит врач-невролог, остеопат.</b><br />
                                    <b>Цюрко Артем Сергеевич</b><br /><br />
                                    <p>Окончил Винницкий Медицинский Университет им. М.И. Пирогова.</p>
                                    <p>Прошел 4-ех годичный курс обучения в испанской школе остеопатии.</p>
                                    <p>Более 10 лет практики в области неврологии, из которых большую часть работал в стационаре.</p>
                                    <p>Имеет опыт лечения сложных заболеваний позвоночника. Использует индивидуальный подход и современные методики.</p> 
                                </li>


                                <li class="g-pricingtable-cta">
                                    <a target="_self" href="#" title="ЗАПИСЬ НА ПРИЕМ ПО ТЕЛЕФОНУ" style="    text-align: center;" class="button ">
                                        <!--<i class="fa fa-arrow-right"></i>-->
                                        ЗАПИСЬ НА ПРИЕМ ПО ТЕЛЕФОНУ<br />(0432) 655-044 <br />(068) 288-44-88<br />(063) 525-58-55
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <div class="slider-clinica-images">
<?php if ($slids_small): ?>
  <?php foreach ($slids_small as $one_small_slide): ?>
                                <div class="slide-image" >
                                    <img src="<?= \app\models\SliderImages::ImageSmallCrop($one_small_slide->img, 450, 450) ?>" style="width: 100%">
                                </div> 

  <?php endforeach; ?>
<?php endif; ?>
                        </div>
                        <div class="clinica-desc">
                            <p>15 05 2016</p>
                            <p class="g-simplecontent-item-content-title">О Центре</p>
                            <p class="index-about-clinic">Основное направление деятельности центра – лечение заболеваний

                                позвоночника. Главная методика лечения: проведение остеопатических

                                сеансов, кинезитерапия и массаж. Приходя к нам, пациенты попадают не

                                просто в медицинский центр, а в атмосферу домашнего уюта.

                                Доброжелательный персонал, профессиональные реабилитологи и

                                массажисты подарят заряд положительных эмоций и хорошего настроения.

                                Мы занимаемся профессиональным лечением позвоночника с удовольствием

                                для нас и наших пациентов.</p>

                            <div>
                                <a target="_self" href="/" title="Подробнее" class="button ">Подробнее</a>
                            </div>

                        </div>


                        <div class="moduletable nopaddingall nomarginall">
                            <div class="g-gridcontent ">
                                <div class="g-grid">
                                    <div class="g-block g-gridcontent-title-desc">
                                    </div>
                                    <div class="g-block g-gridcontent-readmore">
                                    </div>
                                </div>

                                <div class="g-gridcontent-wrapper g-gridcontent-2cols">
                                    <div class="g-gridcontent-item">
                                        <a href="<?= Url::to('/contacts') ?>">
                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                <i class="fa fa-map-marker"></i>
                                                <span class="g-gridcontent-item-title">Как добраться?</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="g-gridcontent-item ">
                                        <a href="<?= Url::to('/site/read-important') ?>">
                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1 ">
                                                <i class="fa fa-exclamation-triangle "></i>
                                                <span class="g-gridcontent-item-title">Важно знать!</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="g-gridcontent-item">
                                        <a href="/comments">
                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                <i class="fa fa-comments"></i>
                                                <span class="g-gridcontent-item-title">Отзывы пациентов</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="g-gridcontent-item">
                                        <a href="<?php echo Url::to("photo") ?>">
                                            <div class="g-gridcontent-item-wrapper g-gridcontent-accent1">
                                                <i class="fa fa-camera"></i>
                                                <span class="g-gridcontent-item-title">Фото Галерея</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            </div>		
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="container" style="    background: #294239;">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="row">
                <div id="comments-block" class="comments-block">
                    <div class="comments g-owlcarousel-layout-testimonial">
                        <div class="g-owlcarousel-item-desc">
                            <i class="fa fa-thumbs-o-up"></i>      
                            Была в центре лечения позвоночника в начале весны. Все понравилось. Врач

                            внимательный. Нормально все объяснил. Понятно. Потом приходила, занималась в

                            зале на тренажерах. Помогло очень. Спасибо.</div>
                        <div class="g-owlcarousel-item-desc">
                            <i class="fa fa-thumbs-o-up"></i>      
                            У меня проблемы с позвоночником давно, сорвал спину, мучился от бесконечных

                            болей. Пришел в городскую больницу врач отправил на МРТ - оказалось грыжи.

                            Сказали, что надо операцию. По совету друга пришел к Артему Сергеевичу. Хожу на

                            остеопатические сеансы, делаю упражнения в зале, прохожу курс физиопроцедур, без

                            лекарств не обошлось, но их не так уж и много. В общем, очень рад, что попал сюда.

                            Пока обошлось без операции.
                        </div>
                        <div class="g-owlcarousel-item-desc">
                            <i class="fa fa-thumbs-o-up"></i>      
                            Добрый день. Хочу поделиться своими впечатлениями от центра, в котором лечила

                            позвоночник: Очень все понравилось. Мило встретили, врач внимательный,

                            выслушал, дал рекомендации, потом сделал несколько манипуляций, абсолютно не

                            больно. Особых проблем у меня нет, просто для профилактики ходила. Впечатления

                            только положительные. А главное - цены приятно радуют. Всем, кто сомневается,

                            советую.
                        </div>
                    </div>
                    <div class="comments-pagination">
                        <ul class="">
                            <li class="selected"><img src="/images3/white.png"></li>
                            <li class=""><img src="/images3/white.png"></li>
                            <li class=""><img src="/images3/white.png"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br />
<br />
<br />

</section>