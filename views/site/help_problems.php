<?php
$this->title = 'Остеопатия для спортсменов решает проблемы:';
?>
<section class="container features section-top">
    <div class="row">
        <div class="col-lg-12 wtite background" style="padding-bottom: 15px;">

            <!--<h1> Для спортсменов</h1>-->
            <h1>Остеопатия для спортсменов решает проблемы:</h1>
            <div style="    float: left;width: 30%;padding-left: 0px;"> 
                <img src="https://doctorkutuzov.ru/upload/54c8dc2516bcb_.jpg" alt="альтернативный текст" style="width: 100%;" >
                <img src="https://static1.squarespace.com/static/552fa8bee4b0b098cbb31f0b/55681710e4b004726f7b73cd/55681711e4b07a367ae8e75d/1432885009751/%D0%BE%D1%81%D1%82_%D0%B4%D0%B5%D1%822.jpg" alt="альтернативный текст" style="width: 100%;" >
            </div>
            <div style="    width: 70%;padding-left: 20px; float: left;">
                <ul class="li-style">
                <li>
                    <i class="fa fa-check"></i> Рубцы, послеоперационные спайки
                </li>
                <li>
                    <i class="fa fa-check"></i> Фасциальные и мышечные боли

                </li>
                <li>
                    <i class="fa fa-check"></i> Бурсит, капсулит, хондрит
                </li>
                <li>
                    <i class="fa fa-check"></i> Вывихи, подвывихи
                </li>
                <li>
                    <i class="fa fa-check"></i> Эпифизит, эпикондилит
                </li>
                <li>
                    <i class="fa fa-check"></i> Грыжа межпозвоночного диска
                </li>
                <li>
                    <i class="fa fa-check"></i> Повреждения связок (растяжения, надрывы)
                </li>
                <li>
                    <i class="fa fa-check"></i> Повреждения колена (крестообразных связок, менисков)
                </li>
                <li>
                    <i class="fa fa-check"></i> Люмбалгия, люмбоишиалгия, межреберная невралгия
                </li>
                <li>
                    <i class="fa fa-check"></i> Миозит, периартрит, периостит
                </li>
                <li>
                   <i class="fa fa-check"></i> Пубалгия
                </li>
                <li>
                    <i class="fa fa-check"></i> Тендиниты
                </li>
                <li>
                    <i class="fa fa-check"></i> Травмы любого генеза и характера
                </li>
                <li>
                    <i class="fa fa-check"></i> Реабилитация после оперативных вмешательств.
                </li>
            </ul>
            </div>

        </div>

        <br />
        <br />
    </div>
</section>
