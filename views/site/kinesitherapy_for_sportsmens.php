<?php
use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Кинезитерапия для спортсменов';


$description = "Кинезитерапия часто становится незаменимой даже для профессиональных спортсменов, привыкших выдерживать сверхвысокие нагрузки.";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url'=>Yii::$app->request->absoluteUrl,
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);

?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">
            <div class="col-md-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Кинезитерапия для спортсменов</h2>
            </div> 
            <div class="col-md-12" style="margin-top: 30px;">
                <div class="col-md-4">
                    <img src="<?= Url::to('/images3/спорт 6.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius:6px;" >
                </div>
                <div class="col-md-8">
                    <p>
                        <span style="font-weight: 600;text-decoration: underline;"> Кинезитерапия</span> часто становится незаменимой даже для профессиональных спортсменов, привыкших
                        выдерживать сверхвысокие нагрузки.
                    </p>
                    <p>Проблема в том, что<b> любой спорт неравномерно тренирует разные мышцы и связки</b>: какие-то развиты
                        превосходно и даже перегружены, а другие, обычно мелкие внутренние мышцы, почти не используются. В то же время за устойчивость структуры тела отвечают
                        как раз эти мелкие мышцы-стабилизаторы.
                    </p>
                    <p>
                        Кинезитерапия позволяет <b>распределять нагрузку на тело более равномерно</b> и задействовать
                        абсолютно все составляющие костно-мышечной системы.
                    </p>
                </div>
            </div>



        </div>   
    </div>
</section>
