<?php

use yii\helpers\Url;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Массажные техники | Остеопатический центр';
$description = "Сеанс массажа – это не только приятный процесс, но и во всех отношениях полезный. ";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'title' => $this->title,
     'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'url' => Yii::$app->request->absoluteUrl,
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);
?>

?>
<section class="container features section-top" style=" padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12 wtite background" style="text-align: justify; padding-bottom: 30px;">

            <div class="col-md-12 col-xs-12" style="margin-top: 30px;">
                <h2 style="font-weight: 600;text-decoration: underline;">Массаж</h2>

            </div>

            <div class="col-md-12 col-xs-12" style="margin-top:30px;">
                <div class="col-md-4" style=" ">
                    <img src="<?= Url::to('/images3/массаж.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius: 6px;" >
                </div>
                <div class="col-md-8" style="">
                    <p>
                        <b>Сеанс массажа </b>– это не только приятный процесс, но и во всех отношениях полезный. <b>Польза массажа многогранна и
                            комплексна.</b> Массаж благоприятно воздействует на кожу, мышцы, суставы,связки и сухожилия, а также на кровеносную,
                        лимфатическую, нервную и дыхательную систему. Особая популярность лечебного массажа связана с тем, что <b>при минимальных ограничениях он показан
                            при множестве заболеваний</b> и проблем со здоровьем. Более того, лечебный массаж, при его грамотном применении, не имеет таких
                        побочных эффектов, как большинство современных лекарственных препаратов.
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12" style="margin-top:30px;">
                <h3 style="padding-top: 0px;"><b>Массажные техники:</b></h3>

                <div class="col-md-4 col-xs-12 mob" style=" ">
                    <img src="<?= Url::to('/images3/массаж 2.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius: 6px;" >
                </div>

                <div class="col-md-6 col-xs-12" style="">
                    <p>
                        Используются для:
                    </p>
                    <ul style="list-style-type: none;">
                        <li>
                            -  глубокой проработки мышц;
                        </li>
                        <li>
                            - активизации крово- и лимфообращения;

                        </li>
                        <li>
                            - улучшения трофики окружающих позвоночный столб мягких тканей;
                        </li>
                        <li>
                            - улучшения питание межпозвоночных дисков.
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 desctop" style=" ">
                    <img src="<?= Url::to('/images3/массаж 2.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius: 6px;" >
                </div>

            </div>

            <div class="col-md-12 col-xs-12" style="margin-top:30px;">

                <div class="col-md-4 col-xs-12 block-with-image" style=" ">
                    <img src="<?= Url::to('/images3/массаж 3.jpg') ?>" alt="альтернативный текст" style="width: 100%; border-radius: 6px;" >
                </div>

                <div class="col-md-6 col-xs-12" style="">
                    <p>
                        <b>В результате использование массажа:</b>
                    </p>
                    <ul class="li-style" style="list-style-type: none;">
                        <li>
                            &#8211 уменьшает и полностью ликвидирует дистрофические явления и застой в позвоночных венах;
                        </li>
                        <li>
                            &#8211 укрепляет мышечного корсета, 
                        </li>
                        <li>
                            &#8211 предупреждает атрофию мышц.
                        </li>
                        <br />
                        <p> Именно массаж является наилучшим методом для уменьшения венозного застоя в воспаленном сегменте и ликвидации дистрофических явлений в позвоночных венах.</p>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
