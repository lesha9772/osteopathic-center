<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use app\components\widgets\AlexMetaTagsWidget;

$this->title = 'Контакти | Остеопатический центр';

$description = "г.Винница, ул. Киевская, 104 (0432) 655-044, (068) 288-44-88, (063) 525-58-55, vinosteoclinic@ukr.net  Остановка троллейбуса и маршруток «ост. Гонты», не переходя дорогу, пройти 200 м в направлении выезда из города, через 200 м увидите вывеску «Остеопатичний центр», войти!";
AlexMetaTagsWidget::widget([
    'domain' => Yii::$app->request->serverName,
    'site_name' => 'Остеопатический центр',
    'keywords' => 'Остеопатия винница, Остеопат винница, Лечение позвоночника винница, лечение грыж позвоночника винница, кинезитерапия винница, мануальная терапия винница, врач-остеопат винница, Остеопатія вінниця, Остеопат вінниця, лікування хребта вінниця, лікування гриж хребта вінниця, кінезітерапія вінниця, мануальна терапія вінниця, лікар-остеопат вінниця',
    'title' => $this->title,
    'url'=>Yii::$app->request->absoluteUrl,
    'description' => $description,
     'image' => (!empty($image_) ? $image_ : yii\helpers\Url::to('/img/logo50x50.png',true))
        ]
);

use yii\web\View;

Yii::$app->view->registerJsFile("/js/index.js");
$this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAhGzVKYgeuBurxOhqcMpYsnDfkGodnCYs&callback=initMap', [], 3);
?>


<section class="container features section-top">
    <div class="row">
        <div class="col-lg-12 wtite background" style="padding-top:65px;padding-bottom: 25px;">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-6">

                    <div class="col-md-12 col-xs-12">
                        <div id="map" style="width:100%; height:300px"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <p>
                        <b>Искать  по адресу: </b> г.Винница, ул. Киевская, 104.
                    </p>
                    <div class="row">
                        <div class="col-md-5">
                            <b>Звонить по телефонам:</b> 
                        </div>
                        <div class="col-md-6">
                            <ul style="list-style: none;
                                padding-left: 0px;"> <li>
                                    (0432) 655-044;
                                </li>
                                <li>
                                    (068) 288-44-88;
                                </li>
                                <li>
                                    (063) 525-58-55
                                </li>
                            </ul>   

                        </div>


                    </div>


                    <p>
                        <b> Писать:</b> <a href="mailto:vinosteoclinic@ukr.net">vinosteoclinic@ukr.net</a>
                    </p>
                    <p>
                        <b>Как проехать?</b>
                    </p>
                    <p>
                        Остановка троллейбуса и маршруток «ост. Гонты», не переходя дорогу, пройти 200 м в направлении выезда из города, через 200 м увидите вывеску «Остеопатичний центр», войти!
                    </p>
                    <p>
                        Маршруты троллейбусов:

                    </p>
                    <ul class="li-style">
                        <li>
                            &#8211; троллейбусом №2 (с ул.Чехова);
                        </li>
                        <li>
                            &#8211; троллейбусом №3 и №8 (с микрорайона Вишенка); 
                        </li>
                        <li>
                            &#8211;  троллейбусом № 6 (с железно-дорожного вокзала);
                        </li>
                        <li>
                            &#8211; троллейбусом № 9 (с ул.Свердлова);
                        </li>
                        <li>
                            &#8211; троллейбусом № 13 (с Аграрного университета).
                        </li>
                    </ul>
                </div>



            </div>


            <div class="col-md-12 col-xs-12">
                <div class="col-md-12 col-xs-12">
                    <p><b>Выглядим так:</b></p>
                </div>

                <div class="col-md-6 col-xs-12">

                    <img src="/img/fasad.jpg" style="width: 100% ; border-radius: 6px;">
                </div>
                <!--                <div class="col-md-6">
                
                                    <img src="/img/fasad.jpg" style="width: 100%; border-radius: 6px;">
                                </div>-->
            </div>
            <!--            <div class="col-md-12 col-xs-12">
                            <p>
                                Схема проезда (карта):
                            </p>
                            <div class="col-md-12 col-xs-12">
            
            
                                <b>тут буде схема проїзда</b>
                                <div id="map" style="width:100%; height:300px"></div>
                            </div>
                        </div>-->




        </div>

        <br />
        <br />
    </div>
</section>