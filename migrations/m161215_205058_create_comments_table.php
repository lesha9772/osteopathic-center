<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m161215_205058_create_comments_table extends Migration {

  /**
   * @inheritdoc
   */
  public function up() {
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    }
    // create table for comments
    $this->createTable('comments', [
        'id' => $this->primaryKey(),
        'img' => $this->string(255)->null(),
        'name' => $this->string(255)->notNull(),
        'text' => $this->text()->notNull(),
        'show' => $this->smallInteger(1)->notNull()->defaultValue('0'),
        'publish_status' => $this->smallInteger(1)->notNull()->defaultValue('0'),
        'created_at' => $this->integer()->notNull(),
        'ip' => $this->string(255)->null(),
        'geo' => $this->string(255)->null(),
            ], $tableOptions);
  }

  /**
   * @inheritdoc
   */
  public function down() {
    $this->dropTable('comments');
  }

}
