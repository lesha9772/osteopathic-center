<?php

use yii\db\Migration;

class m161122_185017_create_table_users extends Migration {

//create roles
//    before migration create rbac
//php yii migrate --migrationPath=@yii/rbac/migrations/
//    date_default_timezone_set('UTC');
  public function up() {
    if ($this->db->driverName === 'mysql') {
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    }


    $this->createTable('{{%user}}', [
        'id' => $this->primaryKey(),
        'username' => $this->string(255)->notNull()->unique(),
        'email' => $this->string(255)->notNull()->unique(),
        'status' => $this->integer(11)->defaultValue(0),
        'auth_key' => $this->string(32)->notNull(),
        'password_hash' => $this->string(255)->notNull(),
        'password_reset_token' => $this->string(255)->unique(),
        'last_visit_at' => $this->integer(11)->defaultValue(0),
            ], $tableOptions);
  }

  public function down() {
    $this->dropTable('user');
  }

}
