<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider_images`.
 */
class m161215_180136_create_slider_images_table extends Migration {

  /**
   * @inheritdoc
   */
  public function up() {
    $this->createTable('slider_images', [
        'id' => $this->primaryKey(),
        'img' => $this->string(255)->notNull(),
        'title' => $this->string(255)->null(),
        'position' => $this->integer(11),
        'slider' => $this->integer(11),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down() {
    $this->dropTable('slider_images');
  }

}
