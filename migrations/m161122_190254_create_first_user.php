<?php

use yii\db\Migration;

class m161122_190254_create_first_user extends Migration {

  public function up() {
    //Create default user admin
    $this->execute($this->getUserSql());
    $auth = Yii::$app->authManager;
    //Set role admin for user admin
    $userRole = Yii::$app->authManager->getRole('admin');
    Yii::$app->authManager->assign($userRole, 1);
  }

  public function down() {
    
  }

  private function getUserSql() {
    $time = time();
    $auth_key = Yii::$app->security->generateRandomString();
    $password_hash = Yii::$app->security->generatePasswordHash('gfhjkm32');
    $password_reset_token = Yii::$app->security->generateRandomString() . '_' . $time;
    return "INSERT INTO {{%user}} ("
            . "`username`,"
            . " `email`,"
            . " `status`,"
            . " `auth_key`,"
            . " `password_hash`,"
            . " `password_reset_token`) "
            . "VALUES ("
            . "'admin',"
            . " 'lesha97721@gmail.com', "
            . " '10', " // статус Активный
            . " '$auth_key',"
            . " '$password_hash',"
            . " '$password_reset_token'
)";
  }

}
