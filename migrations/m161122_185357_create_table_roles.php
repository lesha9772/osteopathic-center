<?php

use yii\db\Migration;

class m161122_185357_create_table_roles extends Migration {

  public function up() {
    // Create roles
    $role = Yii::$app->authManager->createRole('admin');
    $role->description = 'Адмiнiстратор';
    Yii::$app->authManager->add($role);
  }

  public function down() {
    Yii::$app->authManager->removeAllRoles();
  }

  
}
