<?php

use yii\db\Migration;

/**
 * Handles the creation of table `photo`.
 */
class m161214_045405_create_photo_table extends Migration {

  /**
   * @inheritdoc
   */
  public function up() {
    $this->createTable('photo', [
        'id' => $this->primaryKey(),
        'img' => $this->string(255)->notNull(),
        'title' => $this->string(255)->null(),
        'position' => $this->integer(11),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down() {
    $this->dropTable('photo');
  }

}
