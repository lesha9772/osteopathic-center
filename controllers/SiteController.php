<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\SliderImages;
use app\models\Comments;
use yii\data\Pagination;
use yii\filters\HttpCache;

class SiteController extends Controller {

  public function actions() {
    return [
        'error' => [
            'class' => 'yii\web\ErrorAction',
        ],
    ];
  }

  public function behaviors() {
    return [
        'httpCache' => [
            'class' => HttpCache::className(),
            'only' => ['index'],
            'lastModified' => [$this, 'getMaxCustomerTimestamp'],
            'etagSeed' => [$this, 'getMaxCustomerTimestamp'],
        ],
    ];
  }

  public function getMaxCustomerTimestamp($action, $params) {
    return (time() - 24 * 60 * 60);
  }

  public function actionIndex() {

    $slids_small = SliderImages::find()->orderBy('position')->all();

    return $this->render('index', compact('slids_small'));
  }

  public function actionComments() {
    $comments = array();
    $model = new Comments;

    $post = Yii::$app->request->post();

    if ($post) {
      $model->load($post);


      if ($model->validate()) {
        if ($model->save()) {
          Yii::$app->session->setFlash('successfull', 'successfull');
        }
      }
    }

    $model = new Comments;

//    $slids_small = SliderImages::find()->orderBy('position')->all();

    $query = Comments::find()->where(['publish_status' => 0])->orderBy('created_at DESC');
    $countQuery = clone $query;

    $pages = new Pagination([
        'totalCount' => $countQuery->count(),
        'pageSize' => 10,
        'forcePageParam' => false,
        'pageSizeParam' => false,
    ]);

    $comments_list = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

    return $this->render('comments', compact('slids_small', 'model', 'pages', 'comments_list'));
  }

  public function actionPhoto() {
    $model = \app\models\Photo::find()->orderBy('position')->all();

    return $this->render('photo', ['model' => $model]);
  }

  /**
   * Что мы лечим?
   * @return type
   */
  public function actionWhat() {
    return $this->render('what');
  }

  /**
   * Как мы лечим?
   * @return type
   */
  public function actionHow() {
    return $this->render('how');
  }

  /**
   * Остеопатический сеанс
   * @return type
   */
  public function actionOsteopathicsession() {
    return $this->render('osteopathicsession');
  }

  /**
   * Остеопатия: что это?
   * @return type
   */
  public function actionOsteopathywhat() {
    return $this->render('osteopathywhat');
  }

  /**
   * Почему остеопатия?
   * @return type
   */
  public function actionWhyosteopathy() {
    return $this->render('whyosteopathy');
  }

  /**
   * Почему остеопатия?
   * @return type
   */
  public function actionPrinciplesOsteopathy() {
    return $this->render('principles_of_osteopathy');
  }

  /**
   * Почему остеопатия?
   * @return type
   */
  public function actionContraindications() {
    return $this->render('contraindications');
  }

  public function actionPrice() {
    return $this->render('price');
  }

  public function actionKinesitherapy() {
    return $this->render('kinesitherapy');
  }

  public function actionPhysiotherapy() {
    return $this->render('physiotherapy');
  }

  public function actionMassagetechniques() {
    return $this->render('massage_techniques');
  }

  public function actionSpinaltraction() {
    return $this->render('spinal_traction');
  }

  public function actionYogatherapy() {
    return $this->render('yoga_therapy');
  }

  public function actionMedication() {
    return $this->render('medication');
  }

  public function actionSportingFeaturesBodyAndSportsInjury() {
    return $this->render('sporting_features_body_and_sports_injury');
  }

  public function actionWhatAthleteOsteopathicCorrection() {
    return $this->render('what_athlete_osteopathic_correction');
  }

  public function actionKinesitherapyForSportsmens() {
    return $this->render('kinesitherapy_for_sportsmens');
  }

  public function actionOsteopatHelp() {
    return $this->render('osteopat_help');
  }

  public function actionHelpProblems() {
    return $this->render('help_problems');
  }

  public function actionReadImportant() {
    return $this->render('read_important');
  }

  public function actionContacts() {
    return $this->render('contacts');
  }

  /**
   * Обратная связь
   * @return type
   */
  public function actionFeedback() {
    return $this->render('feedback');
  }

  /**
   * Обратная связь
   * @return type
   */
  public function actionLogin() {
    return $this->redirect('/admin/');
  }

  /**
   * Обратная связь
   * @return type
   */
  public function actionSitemap() {
    $this->layout = false;
    return $this->render('sitemap.xml');
  }

}
