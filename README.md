Osteopathic-center на основе Yii 2 Basic Project Template
============================



Требования
-------------------
php 5.4.0 и выше


# Развертывание проекта #

 Клонировать репозиторий

```
git clone git@bitbucket.org:lesha9772/osteopathic-center.git <имя директории в которую выкачаються файлы>

cd <имя директории в которую выкачаються файлы>


```
 Установка библиотек через composer

```
php composer.phar global require "fxp/composer-asset-plugin:^1.2.0"
php composer.phar update -o

```
 Прописать свои настройки для консоли

```
изменить файл app/config/db.php под свои настройки


```
 Накатить базу

```
Запускаем миграции,  2 комманды, после запуска каждой подтверждаем запуск (вводим "YES")  
php yii migrate --migrationPath=@yii/rbac/migrations/
php yii migrate



```