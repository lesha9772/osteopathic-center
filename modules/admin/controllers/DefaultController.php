<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller {

  /**
   * @inheritdoc
   */

  /**
   * Renders the index view for the module
   * @return string
   */
  public function actionIndex() {


    if (Yii::$app->user->can('admin') != true) {
      $this->layout = '@app/modules/admin/views/layouts/login';
      $model = new LoginForm();


      if ($model->load(Yii::$app->request->post()) && $model->login()) {
        return $this->redirect('/admin/dashboard/');
      }
      return $this->render('login', [
                  'model' => $model,
      ]);
    }
    return $this->redirect('/admin/dashboard/');
  }

  /**
   * Logout action.
   *
   * @return string
   */
  public function actionLogout() {
    Yii::$app->user->logout();

    return $this->redirect('/admin/');
  }

}
