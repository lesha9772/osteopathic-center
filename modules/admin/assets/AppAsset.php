<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

  public $basePath = '@webroot';
//    public $basePath = '@app/modules/admin/web';
  public $baseUrl = '@web';
  public $css = [
      'admin_assets/css/custom.css',
      'admin_assets/css/style.css',
      'plugins/font-awesome/css/font-awesome.min.css',
  ];
  public $js = [
      'admin_assets/js/custom.js',
  ];
  public $depends = [
      'yii\web\YiiAsset',
      'yii\bootstrap\BootstrapAsset',
      'yii\bootstrap\BootstrapPluginAsset'
  ];

}
