<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\widgets\yii2_imagecutter\CutterWidget;

/* @var $this yii\web\View */
/* @var $model app\models\SliderImages */
/* @var $form yii\widgets\ActiveForm */
?>
<style>

    .modal-footer > .btn{
        margin-bottom: 0px;
    }
</style>

<div class="x_panel">
    <div class="slider-images-form">
        <div class="x_title">
            <h2><?= ($model->isNewRecord ? Yii::t('new-app', 'Добавления') : Yii::t('new-app', 'Редактирование')) ?> фото <small></small></h2>
            <div class="clearfix"></div>
        </div>
        <?php
        $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'options' => ['enctype' => 'multipart/form-data'],
//                'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'method' => 'post',
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'control-label col-md-3 col-sm-3 col-xs-12',
//                        'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-md-6 col-sm-6 col-xs-12',
                            'error' => '',
                            'hint' => '',
                            'input' => 'form-control col-md-7 col-xs-12',
                        ],
                        'options' => [
                            'tag' => 'div',
                            'class' => 'form-group',
                        ],
                    ]
//                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        ]);
        ?>

        <?=
        $form->field($model, 'img')->widget(CutterWidget::className(), [
            //options
            'useWindowHeight' => true,
            'labelRemove' => false,
            'basePath' => \yii\helpers\Url::to('/img/slider_images/'),
            'cropperOptions' =>
                [
//                'aspectRatio' => '1/1',
                'viewMode' => '3',
                'autoCropArea' => '1',
            ],
        ]);
        ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'position')->textInput() ?>

        <?= $form->field($model, 'slider')->dropDownList(['1' => '"О центре"']) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
