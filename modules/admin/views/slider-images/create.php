<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SliderImages */

$this->title = Yii::t('app', 'Додавання фото');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Фото слайдов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-images-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
