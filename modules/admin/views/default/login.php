<?php
use yii\helpers\Url;

$this->title = 'Login';
?>

<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content">
            <img src="/img/logo.png" style="max-height: 100px;">
                <h1>Login</h1>
                
                <form method="post" action="<?= Url::to('admin/', true) ?>">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->getCsrfToken(); ?>" />
                <div>
                    <input type="text" name="LoginForm[username]" class="form-control" value="" placeholder="Логин" required="" autofocus=""/>
                </div>
                <div>
                    <input type="password" name="LoginForm[password]" class="form-control" value="" placeholder="Пароль" required=""/>
                </div>
                
                
                <div><button type="submit" class="btn btn-default submit">Войти</button></div>
            </form>
                
                

                <div class="clearfix"></div>

                

        </section>
    </div>

</div>