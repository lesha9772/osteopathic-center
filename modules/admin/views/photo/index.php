<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фото';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Добавить фото', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Посмотреть на сайте', yii\helpers\Url::to('/photo'), ['class' => 'btn btn-link', 'target' => '_blank']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
            'id',
                [
                'attribute' => 'img',
                'filter' => false,
                'header' => 'Фото',
                'format' => 'raw',
                'value' => function($data) {

                  return Html::img(\app\models\Photo::ImageSmallCrop($data->img, 150, 150), ['width' => '50']);
                }
            ],
                [
                'attribute' => 'title',
                'label' => 'Title'
            ],
                [
                'attribute' => 'position',
                'label' => 'Позиция'
            ],
                ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
