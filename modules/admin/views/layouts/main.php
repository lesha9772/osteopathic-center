<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\admin\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '/img/logo.png']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="nav-md"> 
        <?php $this->beginBody() ?>


        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
<!--                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?= Url::to('/admin/') ?>" class="site_title">
                                <i class="fa fa-support"></i> <span>A-TKD</span>
                                <img src="/img/logo.png" style=" width: 100%;margin-left: -5px;">
                            </a>
                        </div>-->

                        <div class="clearfix"></div>
                        <?= $this->render('sidebar_menu') ?>
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <?=
                                    Html::beginForm([Url::to("/admin/default/logout")], 'post',['style'=>'margin-top:5px;'])
                                    . Html::submitButton(
                                            'Вихiд', ['class' => 'btn btn-link logout',]
                                    )
                                    . Html::endForm()
                                    ?>
                                </li>

                               
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main" >


                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?= $content ?>


                        </div>
                    </div>


                    <!-- /page content -->

                </div>
                <footer>
                    <div class="pull-right">
<!--                        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>-->
                    </div>
                    <div class="clearfix"></div>
                </footer>

            </div>






            <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
