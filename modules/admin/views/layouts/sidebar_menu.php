<?php

use yii\helpers\Url;
?>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section active">
        <ul class="nav side-menu" style="">
            <li style="    border-bottom: 1px solid rgba(204, 204, 204, 0.33); border: none; background: none;">
                <a href="<?= Url::to('/admin/') ?>" class="site_title" style="height: auto; ">
                                <!--<i class="fa fa-support"></i> <span>A-TKD</span>-->
                    <img src="/img/logo.png" style=" width: 100%;margin-left: -5px;">
                </a>
            </li>
            <li style="    border-bottom: 1px solid rgba(204, 204, 204, 0.33);">
                <a href="<?= Url::to('/admin/photo') ?>"><i class="fa fa-image"></i> Фотогалерея</a>
            </li>
            <li style="    border-bottom: 1px solid rgba(204, 204, 204, 0.33);">
                <a href="<?= Url::to('/admin/slider-images') ?>"><i class="fa fa-picture-o"></i> Слайды</a>
            </li>
            <li style="    border-bottom: 1px solid rgba(204, 204, 204, 0.33);">
                <a href="<?= Url::to('/admin/comments') ?>"><i class="fa fa-comments"></i> Отзывы</a>
            </li>
<!--            <li  style="    border-bottom: 1px solid rgba(204, 204, 204, 0.33);"><a href="<?= Url::to('/admin/lang') ?>"><i class="fa fa-cog"></i> Языки</a>
            </li>-->
        </ul>
    </div>

</div>