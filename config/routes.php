<?php

return [
    '' => 'site/index',
    'sitemap.xml' => 'site/sitemap',
    '<action:\w+>' => 'site/<action>',
    '<controller:\w+>/' => '<controller:\w+>/index',
    '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
//    '<module:\w+>/<controller:(^slider-images|^news)>/<slug>' => '<module>/<controller>/index',
    '<module:\w+>/<controller:[a-z-]+>/<action:[a-z-]+>' => '<module>/<controller>/<action>',
];
