<?php

$params = array_merge(
        require(__DIR__ . '/params.php')
);

//$params = require(__DIR__ . '/params.php');
//use \yii\web\Request;
//$baseUrl = str_replace('/web', '', (new Request)->getBaseUrl());

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru',
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
        ],
    ],
    'homeUrl' => '/',
    'components' => [
         'authManager' => [
//            'class' => 'yii\rbac\PhpManager', // or use 'yii\rbac\DbManager'
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache' //Включаем кеширование 
        ],
       'i18n' => [
            'translations' => [
                'new-app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],
        'request' => [
            'baseUrl' => '',
//            'baseUrl'=> '',
//             'baseUrl' => $baseUrl,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6d94oZUa7YCq2jhgHu4tmpCNEPc_gI2P',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
//            'class' => 'app\components\LangUrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'baseUrl' => '/',
            'rules' => require(__DIR__ . '/routes.php'),
        ],
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => '6d94oZUa7YCq2jhgHu4tmpCNEPc_gI2P',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        //...
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
