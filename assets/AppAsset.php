<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
//      'css/styles.css',
      'css/font-awesome.min.css',
      'css/blueimp-gallery.min.css',
      'css/site.css',
      'css/slider.css',
      'css/animate.css',
      'css/>992.css',
//      'css/>768and<992.css',
      'css/<768.css',
  ];
  public $js = [
      'js/jquery.blueimp-gallery.min.js',
      'js/functions.js'
  ];
  public $depends = [
      'yii\web\YiiAsset',
      'yii\bootstrap\BootstrapAsset',
      'yii\bootstrap\BootstrapPluginAsset',
  ];

}
