<?php

namespace app\models;

use Yii;
use app\components\widgets\yii2_imagecutter\behaviors\CutterBehavior;

/**
 * This is the model class for table "slider_images".
 *
 * @property integer $id
 * @property string $img
 * @property string $title
 * @property integer $position
 * @property integer $slider
 */
class SliderImages extends \yii\db\ActiveRecord {

  public static $BASE_DIR = 'slider_images';
  public static $BASE_PATH = '@app/web/img/';

  public function behaviors() {
    return [
        'image' => [
            'class' => CutterBehavior::className(),
            'attributes' => ['img'],
            'baseDir' => self::$BASE_DIR,
            'basePath' => self::$BASE_PATH,
        ],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'slider_images';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
            [['title'], 'required'],
            [['position', 'slider'], 'integer'],
            [['img', 'title'], 'string', 'max' => 255],
            ['position', 'default', 'value' => 999999999],
//            [['img'], 'file', 'extensions' => 'jpg, jpeg, gif,  JPG, GIF, PNG, JPEG', 'maxSize' => 1024000, 'tooBig' => 'Limit is 1мб'],
        [['img'], 'file', 'extensions' => 'jpg, jpeg, gif,  JPG, GIF, PNG, JPEG', 'maxSize' => 1024000, 'tooBig' => 'Limit is 1мб'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
        'id' => Yii::t('app', 'ID'),
        'img' => Yii::t('app', 'Фото'),
        'title' => Yii::t('app', 'Название'),
        'position' => Yii::t('app', 'Позиция'),
        'slider' => Yii::t('app', 'Слайдер'),
    ];
  }

  static public function ImageSmallCrop($imgName, $width = 100, $height = 100) {
    $smallDir = 'small/';
    $smallImage = \app\components\widgets\yii2_imagecutter\CutterWidget::getSmallImage($imgName, self::$BASE_PATH, self::$BASE_DIR, $smallDir, $width, $height);
    return $smallImage ? \yii\helpers\Url::to('/img' . $smallImage) : \yii\helpers\Url::to('' . $smallImage);
  }

}
