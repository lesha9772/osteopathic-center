<?php

namespace app\models;

use Yii;
use app\components\behaviors\IpBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\components\behaviors\GeoBehavior;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property string $img
 * @property string $name
 * @property string $text
 * @property integer $show
 * @property integer $publish_status
 * @property integer $created_at
 * @property string $ip
 * @property string $geo
 */
class Comments extends ActiveRecord {

  public $default_country = 'noname';

  public function behaviors() {
    return [
            [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
            ],
        ],
//        'EmailBehavior' => [
//            'class' => EmailBehavior::className(),
//            'attributes' => [
//                ActiveRecord::EVENT_AFTER_INSERT => 'sendEmail',
//            ]
//        ],
        'IpBehavior' => [
            'class' => IpBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => 'ip',
            ]
        ],
        'GeoBehavior' => [
            'class' => GeoBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => 'geo',
            ],
            'default_country' => $this->default_country,
        ],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'comments';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
            [['name', 'text'], 'required'],
            [['text'], 'string'],
            [['show', 'publish_status', 'created_at'], 'integer'],
            [['img', 'name', 'ip', 'geo'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
        'id' => Yii::t('app', 'ID'),
        'img' => Yii::t('app', 'Фото'),
        'name' => Yii::t('app', 'имя'),
        'text' => Yii::t('app', 'Сообщение'),
        'show' => Yii::t('app', 'Show'),
        'publish_status' => Yii::t('app', 'Publish Status'),
        'created_at' => Yii::t('app', 'Created At'),
        'ip' => Yii::t('app', 'Ip'),
        'geo' => Yii::t('app', 'Geo'),
    ];
  }

}
